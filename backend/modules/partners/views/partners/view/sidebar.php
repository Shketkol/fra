<div class="col-md-2">
    <div class="panel wallet-widgets">
        <ul class="wallet-list">
            <li><a href="<?=\yii\helpers\Url::to(['/finance/finance/index', 'id' => $model->id])?>">
                    <div class="text-center">
                        Финансы
                    </div>
                </a>
            </li>
            <li><a href="<?=\yii\helpers\Url::to(['/royalty/royalty/index', 'id' => $model->id])?>">
                    <div class="text-center">
                        Расчет роялти
                    </div>
                </a>
            </li>
            <li><a href="<?=\yii\helpers\Url::to(['/income/income/index', 'id' => $model->id])?>">
                    <div class="text-center">
                        Доход/Расход
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>