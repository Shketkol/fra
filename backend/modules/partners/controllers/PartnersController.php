<?php

namespace backend\modules\partners\controllers;

use backend\components\controllers\AdminController;
use backend\models\ProductSearch;
use backend\models\UsersSearch;
use common\models\Customers;
use common\models\Users;
use common\models\UsersProfile;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class PartnersController extends AdminController
{

    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search_user(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionProduct($id){
        $model = $this->loadModel($id);
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('products', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function loadModel($id)
    {
        $model = Users::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }

}



