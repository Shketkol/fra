<?
$this->title = 'Доход/Расход';
$this->params['breadcrumbs'][] = array('label' => $this->title, 'url' => ['index', 'id' => $user->id]);$this->params['breadcrumbs'][] = 'Поля расхода';
?>

<?= $this->render('@app/modules/partners/views/partners/view/head', array('model' => $user)) ?>

<div class="row">

    <?= $this->render('@app/modules/partners/views/partners/view/sidebar', array('model' => $user)) ?>

    <div class="col-md-10">
        <div class="panel">
            <? echo \yii\helpers\Html::a('<i class="fa fa-plus m-r-5"></i> Добавить ', \yii\helpers\Url::to(['/income/income/out-create', 'id' => $user->id]), [
                'class' => 'btn btn-success m-l-10 waves-effect waves-light'
            ]) ?>
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 80px;']
                        ],
                        'name',
                        [
                            'attribute' => 'products.name',
                        ],
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
                            'template' => '{update} {delete}',
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return \yii\helpers\Html::a('<i class="ti-pencil" aria-hidden="true"></i>', \yii\helpers\Url::to(['out-update', 'id' => $model->id]), [
                                        'data-original-title' => 'Редактировать',
                                        'data-toggle' => 'tooltip',
                                        'class' => 'btn btn-sm btn-icon btn-info btn-outline no-pjax'
                                    ]);
                                },
                                'delete' => function ($url, $model) {
                                    return \yii\helpers\Html::a('<i class="ti-trash" aria-hidden="true"></i>', \yii\helpers\Url::to(['out-delete', 'id' => $model->id]), [
                                        'data-original-title' => 'Редактировать',
                                        'data-toggle' => 'tooltip',
                                        'class' => 'btn btn-sm btn-icon btn-info btn-outline no-pjax'
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>