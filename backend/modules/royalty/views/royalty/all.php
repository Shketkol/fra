<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Расчет роялти';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="white-box">
                <div role="tabpanel" class="tab-pane" id="home">
                    <div class="col-md-2 pull-right">
                    <a href="<?=Url::to(['/royalty/royalty/file', 'year' => $year, 'month' => $month])?>" class="btn btn-block btn-danger">Сформировать файл</a>
                    </div>
                    <div class="col-md-2 pull-right">
                        <div class="form-group">
                            <select class="form-control js_royalty">
                                <option value="01" <?= ($month == '01') ? 'selected' : null ?>>январь</option>
                                <option value="02" <?= ($month == '02') ? 'selected' : null ?>>февраль</option>
                                <option value="03" <?= ($month == '03') ? 'selected' : null ?>>март</option>
                                <option value="04" <?= ($month == '04') ? 'selected' : null ?>>апрель</option>
                                <option value="05" <?= ($month == '05') ? 'selected' : null ?>>май</option>
                                <option value="06" <?= ($month == '06') ? 'selected' : null ?>>июнь</option>
                                <option value="07" <?= ($month == '07') ? 'selected' : null ?>>июль</option>
                                <option value="08" <?= ($month == '08') ? 'selected' : null ?>>август</option>
                                <option value="09" <?= ($month == '09') ? 'selected' : null ?>>сентябрь</option>
                                <option value="10"<?= ($month == 10) ? 'selected' : null ?>>октябрь</option>
                                <option value="11" <?= ($month == 11) ? 'selected' : null ?>>ноябрь</option>
                                <option value="12" <?= ($month == 12) ? 'selected' : null ?>>декабрь</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 pull-right">
                        <div class="form-group">
                            <select class="form-control js_royalty">
                                <? foreach ($years as $value) { ?>
                                    <option value="<?= date('Y', strtotime($value->date)) ?>" <?= ($year == date('Y', strtotime($value->date))) ? 'selected' : null ?>><?= date('Y', strtotime($value->date)) ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                    <div class="text-left pull-left" style="margin-top: -10px;">
                        <h1>Роялти, итого к выплате:</h1>
                    </div>
                    <div class="text-right">
                        <h1><?= $all ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 80px;']
                        ],
                        [
                            'attribute' => 'users_id',
                            'value' => 'users.name',
                            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Users::find()->asArray()->all(), 'id', 'name'),

                        ],
                        'value',
                        [
                            'attribute' => 'agree',
                            'content' => function ($model) {
                                if ($model->agree == 0) {
                                    return Html::tag('span', 'несогласен', ['class' => 'label label-warning label-rouded text-uppercase']);
                                } else {
                                    return Html::tag('span', 'согласен', ['class' => 'label label-success label-rouded text-uppercase']);
                                }
                            },
                            'filter' => array(
                                0 => 'несогласен',
                                1 => 'согласен'
                            )

                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
