<?php

namespace backend\modules\royalty\controllers;

use backend\components\controllers\AdminController;
use backend\models\RoyaltySearch;
use common\models\Config;
use common\models\Leads;
use common\models\Royalty;
use common\models\Users;
use common\models\Products;
use Yii;

class RoyaltyController extends AdminController
{

    public function actionIndex($id)
    {
        $user = Users::findOne(['id' => $id]);
        if(!isset($_GET['search'])){
            $year = date('Y', time());
            $month = date('m', time());
        }else {
            $test = explode('-', $_GET['search']);
            $year = $test[1];
            $month = $test[0];
        }
        $royalty = 0;
        foreach (Products::find()->all() as $value) {
            $royalty = $royalty + Royalty::getRoyaltyForUser($user->id, $value, $year, $month);
        }

        $years = Leads::find()
            ->select('YEAR(date) as date')
            ->distinct()
            ->where(['users_id' => $user->id])
            ->orderBy('date DESC')
            ->all();

        $current = Royalty::find()
            ->where(['users_id' => $user->id])
            ->andFilterWhere(['like', 'created_at',  $year.'-'.$month])
            ->one();

        return $this->render('index', [
            'royalti' => $royalty,
            'month' => $month,
            'year' => $year,
            'years' => $years,
            'current' => $current,
            'user' => $user
        ]);
    }

    public function actionAll(){
        if(!isset($_GET['search'])){
            $year = date('Y', time());
            $month = date('m', time());
        }else {
            $test = explode('-', $_GET['search']);
            $year = $test[1];
            $month = $test[0];
        }

        $searchModel = new RoyaltySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $year, $month);

        $all = Royalty::find()
            ->select('SUM(value) as value')
            ->andFilterWhere(['like', 'created_at',  $year.'-'.$month])
            ->one();
        $all = (empty($all['value'])) ? 0.00 : $all['value'];

        $years = Leads::find()
            ->select('YEAR(date) as date')
            ->distinct()
            ->orderBy('date DESC')
            ->all();

        return $this->render('all', [
            'all' => $all,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'years' => $years,
            'year' => $year,
            'month' => $month
        ]);
    }

    public function actionFile(){
        if(isset($_GET['year'])){
            $year = $_GET['year'];
            $month = $_GET['month'];

            $all = Royalty::find()
                ->andFilterWhere(['like', 'created_at',  $year.'-'.$month])
                ->all();
            $path = $_SERVER['DOCUMENT_ROOT'].'/export/'.$year.'-'.$month.'.csv';
            $file1 = fopen($path, 'w');
            $str = "Счет корреспондента;Сумма;Назначение;Пользователь;Город;Примечание;Комментарий"."\n";
            fwrite($file1, $str);
            foreach ($all as $output){
                $config = Config::findOne(['id' => 1]);
                $str2 = $output->users->usersProfile->content['bank_cornumber'].';'.$output->value.';'.'Списание роялти за '.$month.' '.$year.' на основании договора'.';'.$output->users->name.';'.$output->users->usersProfile->city.';';
                if($output->agree == 0){
                    $str2 = $str2.'не согласен'.";";
                }else {
                    $str2 = $str2.'согласен'.";";
                }
                if(!empty($output->comment)){
                    $str2 = $str2.$output->comment."\n";
                }else {
                    $str2 = "\n";
                }
                fwrite($file1, $str2);
            }
            fclose($file1);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.$year.'-'.$month.'.csv"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            readfile($path);
            Yii::$app->end();
        }
    }

}
