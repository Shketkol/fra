<?php

namespace backend\modules\users;

use backend\modules\users\assets\UsersAsset;
use Yii;
use yii\web\View;

/**
 * users module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\users\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        //register assets
        Yii::$app->view->on(View::EVENT_BEFORE_RENDER, function ($event) {
            UsersAsset::register($event->sender);
        });
        // custom initialization code goes here
        parent::init();

        // custom initialization code goes here
    }
}
