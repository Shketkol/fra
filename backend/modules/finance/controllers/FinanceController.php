<?php

namespace backend\modules\finance\controllers;

use backend\components\controllers\AdminController;
use common\models\Leads;
use common\models\LeadsSearch;
use common\models\Users;
use yii\filters\AccessControl;
use Yii;
use yii\web\User;

/**
 * Finance controller for the `finance` module
 */
class FinanceController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id)
    {
        
        if(!isset($_GET['start'])){
            $start = date("Y-m-1", time());
        }
        else{
            $start = date("Y-m-d", $_GET['start']);
        }
        
        if(!isset($_GET['end'])){
            $end = date("Y-m-t", time());
        }
        else{
            $end = date("Y-m-d", $_GET['end']);
        }
        
        if(!isset($_GET['leadStatus'])){
            $leadStatus = 0;
        }
        else{
            $leadStatus = $_GET['leadStatus'];
        }
        
        if(!isset($_GET['search'])){
            $year = date('Y', time());
            $month = date('m', time());  
        }else {
            $test = explode('-', $_GET['search']);
            $year = $test[1];
            $month = $test[0];
        }
        
        $all_online = Leads::find()
            ->select('SUM(price) as value')
            ->where(['users_id' => $id, 'type' => Leads::TYPE_ONLINE])
            ->andFilterWhere(['between', 'date',  $start, $end]);
        
        if($leadStatus == 1) $all_online = $all_online->andFilterWhere(['status' => 'paid']);
        else if($leadStatus == 2) $all_online = $all_online->andFilterWhere(['status' => 'prepaid']);
        
        $all_online = $all_online
            ->asArray()
            ->one();
        $all_online = (empty($all_online['value'])) ? '0.00' : $all_online['value'];

        $all_offline = Leads::find()
            ->select('SUM(price) as value')
            ->where(['users_id' => $id, 'type' => Leads::TYPE_OFFLINE])
            ->andFilterWhere(['between', 'date',  $start, $end])
            ->asArray()
            ->one();
        $all_offline = (empty($all_offline['value'])) ? '0.00' : $all_offline['value'];


        $years = Leads::find()
            ->select('YEAR(date) as date')
            ->distinct()
            ->where(['users_id' => $id])
            ->orderBy('date DESC')
            ->all();
        
        return $this->render('index', [
            'all_online' => $all_online,
            'all_offline' => $all_offline,
            'user' => Users::findOne(['id' => $id]),
            'year' => $year,
            'month' => $month,
            'years' => $years,
            'start' => $start,
            'end' => $end,
            'leadStatus' => $leadStatus
        ]);
    }

    public function actionView($id, $product, $type){
        
        if(!isset($_GET['start'])){
            $start = date("Y-m-1", time());
        }
        else{
            $start = date("Y-m-d", $_GET['start']);
        }
        
        if(!isset($_GET['end'])){
            $end = date("Y-m-t", time());
        }
        else{
            $end = date("Y-m-d", $_GET['end']);
        }
        
        if(!isset($_GET['leadStatus'])){
            $leadStatus = 0;
        }
        else{
            $leadStatus = $_GET['leadStatus'];
        }
        
        $searchModel = new LeadsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id, $product, $type, $start, $end, $leadStatus);
        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'start' => $start,
            'end' => $end,
            'leadStatus' => $leadStatus
        ]);
    }
}
