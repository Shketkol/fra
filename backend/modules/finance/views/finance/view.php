<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Финансы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row" style="margin-bottom:25px">
    <div class="col-md-3">
        <label>Дата</label>
        <input type="text" value="<?=$start?> - <?=$end?>" name="daterange" class="form-control" />
    </div>
    <div class="col-md-3">
        <label>Статус</label>
        <select name="leadStatus" class="form-control ls">
            <option value="0" <?=($leadStatus == 0) ? 'selected' : null?>>Все</option>
            <option value="1" <?=($leadStatus == 1) ? 'selected' : null?>>Оплата</option>
            <option value="2" <?=($leadStatus == 2) ? 'selected' : null?>>Предоплата</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel wallet-widgets">
            <?php \yii\widgets\Pjax::begin(); ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => false,
                'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                'tableOptions' => [
                    'class' => 'table table-hover manage-u-table'
                ],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['class' => 'text-center'],
                        'headerOptions' => ['class' => 'text-center', 'style' => 'width: 80px;']
                    ],
                    //'amo_id',
                    [
                        'attribute' => 'amo_id',
                        'content' => function($model) {
                            return '<a href="https://likebusiness.amocrm.ru/leads/detail/'.$model->amo_id.'?compact=yes">'.$model->amo_id.'</a>';
                        },

                    ],
                    'name_lead',
                    'contact',
                    'state',
                    'price',
                    'email',
                    'phone',
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>

<?
$script = <<< JS
(function () {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
            new CBPFWTabs(el);
        });
    })();
// // jQuery(document).ready(function () {
//         $("input[name='tch3']").TouchSpin({
//             min: 1,
//             max: 9999,
//             buttondown_class: 'btn btn-info btn-outline',
//             buttonup_class: 'btn btn-info btn-outline',
//         });

function search_range(start, end) {
    //var pattern = /(&search=\d+-\d+)|(&search=\d+-\d+).*/g;
    //var url = window.location.search;
    //window.location = url + '&start='+start+'&end='+end;
    
    //var pattern_start = /(&start=\d+-\d+)|(&start=\d+-\d+).*/g;
    //var pattern_end = /(&end=\d+-\d+)|(&end=\d+-\d+).*/g;
    
    var urlX = new URL(window.location.href);
    var id = urlX.searchParams.get("id");
    var product = urlX.searchParams.get("product");
    var type = urlX.searchParams.get("type");
    var leadStatus = urlX.searchParams.get("leadStatus");
    
    if(leadStatus != null) window.location = window.location.href.split("?")[0] + '?id=' + id + '&product=' + product + '&type=' + type + '&start=' + start + '&end=' + end + '&leadStatus=' + leadStatus;
    else  window.location = window.location.href.split("?")[0] + '?id=' + id + '&product=' + product + '&type=' + type + '&start=' + start + '&end=' + end;
    
    /*if (window.location.search.search(pattern) < 0){
        window.location.search = url + '&start='+start+'&end='+end;
    } else {
        window.location.search = url.replace(pattern, '&search='+inp);
    }*/
}

$('input[name="daterange"]').daterangepicker(
    {
        locale: {
          format: 'YYYY-MM-DD'
        },
        //startDate: '2013-01-01',
        //endDate: '2013-12-31'
    }, 
    function(start, end, label) {
        //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        search_range(parseInt(start/1000), parseInt(end/1000));
    }
);

$('.ls').on('change', function(){
    var urlX = new URL(window.location.href);
    var id = urlX.searchParams.get("id");
    var start = urlX.searchParams.get("start");
    var end = urlX.searchParams.get("end");
    var leadStatus = $(this).val();
    var product = urlX.searchParams.get("product");
    var type = urlX.searchParams.get("type");
    
    if(start != null & end != null){
        window.location = window.location.href.split("?")[0] + '?id=' + id + '&product=' + product + '&type=' + type + '&start=' + start + '&end=' + end + '&leadStatus=' + leadStatus;
    }
    else{
        window.location = window.location.href.split("?")[0] + '?id=' + id + '&product=' + product + '&type=' + type + '&leadStatus=' + leadStatus;
    }
});

JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
