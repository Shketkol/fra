<?
use \yii\helpers\Html;
use \yii\helpers\Url;

$this->title = 'Финансы';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('@app/modules/partners/views/partners/view/head', array('model' => $user)) ?>

<div class="row">

    <?= $this->render('@app/modules/partners/views/partners/view/sidebar', array('model' => $user)) ?>

    <div class="col-md-10">
        <div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel wallet-widgets">
                        <div class="panel-body">
                        <!--
                        <div class="row">
                           <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control js_royalty">
                                    <option value="01" <?=($month == '01') ? 'selected' : null?>>январь</option>
                                    <option value="02" <?=($month == '02') ? 'selected' : null?>>февраль</option>
                                    <option value="03" <?=($month == '03') ? 'selected' : null?>>март</option>
                                    <option value="04" <?=($month == '04') ? 'selected' : null?>>апрель</option>
                                    <option value="05" <?=($month == '05') ? 'selected' : null?>>май</option>
                                    <option value="06" <?=($month == '06') ? 'selected' : null?>>июнь</option>
                                    <option value="07" <?=($month == '07') ? 'selected' : null?>>июль</option>
                                    <option value="08" <?=($month == '08') ? 'selected' : null?>>август</option>
                                    <option value="09" <?=($month == '09') ? 'selected' : null?>>сентябрь</option>
                                    <option value="10"<?=($month == 10) ? 'selected' : null?>>октябрь</option>
                                    <option value="11" <?=($month == 11) ? 'selected' : null?>>ноябрь</option>
                                    <option value="12" <?=($month == 12) ? 'selected' : null?>>декабрь</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control js_royalty">
                                    <?if(!empty($years)){?>
                                    <?foreach ($years as $value){?>
                                        <option value="<?=date('Y', strtotime($value->date))?>" <?=($year == date('Y', strtotime($value->date))) ? 'selected' : null?>><?=date('Y', strtotime($value->date))?></option>
                                    <? }?>
                                    <? } else {?>
                                        <option value="<?=date('Y', time())?>" <?=($year == date('Y', time())) ? 'selected' : null?>><?=date('Y', time())?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                        </div> 
                           -->  
                            <div class="text-left pull-left">
                                <h3>Безналичная выручка:</h3>
                            </div>
                            <div class="text-right">
                                <h3><?= $all_online ?></h3>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Дата</label>
                                    <input type="text" value="<?=$start?> - <?=$end?>" name="daterange" class="form-control" />
                                </div>
                                <div class="col-md-6">
                                    <label>Статус</label>
                                    <select name="leadStatus" class="form-control ls">
                                        <option value="0" <?=($leadStatus == 0) ? 'selected' : null?>>Все</option>
                                        <option value="1" <?=($leadStatus == 1) ? 'selected' : null?>>Оплата</option>
                                        <option value="2" <?=($leadStatus == 2) ? 'selected' : null?>>Предоплата</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <ul class="wallet-list">
                            <? foreach (\common\models\Products::find()->all() as $value) { ?>
                                <li><a href="<?= Url::to(['/finance/finance/view', 'id' => $user->id, 'product' => $value->id, 'type' => \common\models\Leads::TYPE_ONLINE]) ?>">
                                        <div class="text-left pull-left">
                                            <?= $value->name ?>:
                                        </div>
                                        <div class="text-right">
                                            <?= \common\models\Leads::getSumProductUserOnline($user->id, $value->id, $year, $month, $start, $end, $leadStatus) ?>
                                        </div>
                                    </a>
                                </li>
                            <? } ?>
                        </ul>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel wallet-widgets">
                        <div class="panel-body">
                            <div class="text-left pull-left">
                                <h1>Наличная выручка:</h1>
                            </div>
                            <div class="text-right">
                                <h1><?= $all_offline ?></h1>
                            </div>
                        </div>
                        <ul class="wallet-list">
                            <? foreach (\common\models\Products::find()->all() as $value) { ?>
                                <li><a href="<?= Url::to(['/finance/finance/view', 'id' => $user->id, 'product' => $value->id, 'type' => \common\models\Leads::TYPE_OFFLINE]) ?>">
                                        <div class="text-left pull-left">
                                            <?= $value->name ?>:
                                        </div>
                                        <div class="text-right">
                                            <?= \common\models\Leads::getSumProductUserOffline($user->id, $value->id, $year, $month, $start, $end) ?>
                                        </div>
                                    </a>
                                </li>
                            <? } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
$script = <<< JS
(function () {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
            new CBPFWTabs(el);
        });
    })();
// // jQuery(document).ready(function () {
//         $("input[name='tch3']").TouchSpin({
//             min: 1,
//             max: 9999,
//             buttondown_class: 'btn btn-info btn-outline',
//             buttonup_class: 'btn btn-info btn-outline',
//         });

function search_range(start, end) {
    var urlX = new URL(window.location.href);
    var id = urlX.searchParams.get("id");
    var leadStatus = urlX.searchParams.get("leadStatus");
    
    if(leadStatus != null){
        window.location = window.location.href.split("?")[0] + '?id=' + id + '&start=' + start + '&end=' + end + '&leadStatus=' + leadStatus;
    }
    else{
        window.location = window.location.href.split("?")[0] + '?id=' + id + '&start=' + start + '&end=' + end;
    }
}

$('input[name="daterange"]').daterangepicker(
    {
        locale: {
          format: 'YYYY-MM-DD'
        },
    }, 
    function(start, end, label) {
        search_range(parseInt(start/1000), parseInt(end/1000));
    }
);

$('.ls').on('change', function(){
    var urlX = new URL(window.location.href);
    var id = urlX.searchParams.get("id");
    var start = urlX.searchParams.get("start");
    var end = urlX.searchParams.get("end");
    var leadStatus = $(this).val();
    
    if(start != null & end != null){
        window.location = window.location.href.split("?")[0] + '?id=' + id + '&start=' + start + '&end=' + end + '&leadStatus=' + leadStatus;
    }
    else{
        window.location = window.location.href.split("?")[0] + '?id=' + id + '&leadStatus=' + leadStatus;
    }
});

JS;
$this->registerJs($script, yii\web\View::POS_END);
?>


