<?php
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;
/* @var $this yii\web\View */

$this->title = 'Франшиза';
?>

<div class="row" style="margin-bottom: 25px;">
    <div class="col-md-3">
        <div class="form-group">
            <select class="form-control js_index">
                <option value="01" <?=($month == '01') ? 'selected' : null?>>январь</option>
                <option value="02" <?=($month == '02') ? 'selected' : null?>>февраль</option>
                <option value="03" <?=($month == '03') ? 'selected' : null?>>март</option>
                <option value="04" <?=($month == '04') ? 'selected' : null?>>апрель</option>
                <option value="05" <?=($month == '05') ? 'selected' : null?>>май</option>
                <option value="06" <?=($month == '06') ? 'selected' : null?>>июнь</option>
                <option value="07" <?=($month == '07') ? 'selected' : null?>>июль</option>
                <option value="08" <?=($month == '08') ? 'selected' : null?>>август</option>
                <option value="09" <?=($month == '09') ? 'selected' : null?>>сентябрь</option>
                <option value="10"<?=($month == 10) ? 'selected' : null?>>октябрь</option>
                <option value="11" <?=($month == 11) ? 'selected' : null?>>ноябрь</option>
                <option value="12" <?=($month == 12) ? 'selected' : null?>>декабрь</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <select class="form-control js_index">
                <?if(!empty($years)){?>
                    <?foreach ($years as $value){?>
                        <option value="<?=date('Y', strtotime($value->date))?>" <?=($year == date('Y', strtotime($value->date))) ? 'selected' : null?>><?=date('Y', strtotime($value->date))?></option>
                    <? }?>
                <? } else {?>
                    <option value="<?=date('Y', time())?>" <?=($year == date('Y', time())) ? 'selected' : null?>><?=date('Y', time())?></option>
                <? }?>
            </select>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <div class="row row-in">
                <div class="col-lg-6 col-sm-6 row-in-br">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-success"><i class=" ti-shopping-cart"></i></span>
                        </li>
                        <li class="col-last"><h3 class="counter text-right m-t-15"><?=$users?></h3></li>
                        <li class="col-middle">
                            <h4>Количество франчайзи</h4>
                        </li>

                    </ul>
                </div>
                <div class="col-lg-6 col-sm-6  b-0">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-warning"><i class="fa fa-dollar"></i></span>
                        </li>
                        <li class="col-last"><h3 class="counter text-right m-t-15"><?=(empty($sum['price'])) ? 0.00 : $sum['price']?></h3></li>
                        <li class="col-middle">
                            <h4>Сумма выручки</h4>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h1 class="box-title">Рейтинг франчайзи</h1>
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],

                        'name',
                        [
                            'label' => 'Город',
                            'content' => function($model) {
                                return '<a href="'.Url::to(['/finance/finance/index', 'id' => $model['id']]).'">'.$model['city'].'</a>';
                            },

                        ],
                        'email',
//                        'price',
                        [
                            'label' => ' размер роялти',
                            'attribute' => 'royalti'
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
<?
$script = <<< JS

function search_range(start, end) {
    var urlX = new URL(window.location.href);
    window.location = window.location.href.split("?")[0] + '?start=' + start + '&end=' + end;
}

$('input[name="daterange"]').daterangepicker(
    {
        locale: {
          format: 'YYYY-MM-DD'
        },
        //startDate: '2013-01-01',
        //endDate: '2013-12-31'
    }, 
    function(start, end, label) {
        //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        search_range(parseInt(start/1000), parseInt(end/1000));
    }
);

JS;
$this->registerJs($script, yii\web\View::POS_END);
?>