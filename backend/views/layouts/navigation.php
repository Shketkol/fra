<?
use \yii\helpers\Url;
?>
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Навигация</span></h3> </div>
        <ul class="nav" id="side-menu">
            <li> <a href="<?=\yii\helpers\Url::to(['/site/index'])?>" class="waves-effect"><i class="mdi mdi-gauge fa-fw"></i><span class="hide-menu">Главная панель</span></a></li>
            <li> <a href="<?=\yii\helpers\Url::to(['/products/products/index'])?>" class="waves-effect"><i class="mdi mdi-book-multiple fa-fw"></i><span class="hide-menu">Продукты</span></a></li>
            <li> <a href="<?=\yii\helpers\Url::to(['/partners/partners/index'])?>"><i class="mdi mdi-account-star fa-fw"></i><span class="hide-menu">Франчайзи</span></a> </li>
            <li><a href="<?=Url::to(['/royalty/royalty/all'])?>" class="waves-effect"><i class="mdi mdi-clipboard-flow fa-fw"></i> <span class="hide-menu">Роялти</span></a></li>
            <li><a href="<?=Url::to(['/config/default/index'])?>" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Настройки</span></a></li>
            <li><a href="<?=Url::to(['/logout'])?>" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">Выйти</span></a></li>
        </ul>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Left Sidebar -->
<!-- ============================================================== -->