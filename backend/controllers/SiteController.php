<?php
namespace backend\controllers;

use backend\components\controllers\AdminController;
use backend\models\UsersRaitingSearch;
use common\models\Leads;
use common\models\Users;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends AdminController
{
//    /**
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
////                        'actions' => ['login', 'error'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout', 'index'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // filter date
//        if(!isset($_GET['start'])){
//            $start = date("Y-m-1", time());
//        }
//        else{
//            $start = date("Y-m-d", $_GET['start']);
//        }
//
//        if(!isset($_GET['end'])){
//            $end = date("Y-m-t", time());
//        }
//        else{
//            $end = date("Y-m-d", $_GET['end']);
//        }

        if(!isset($_GET['search'])){
            $year = date('Y', time());
            $month = date('m', time());
            $start = date("Y-m-1", time());
            $end = date("Y-m-t", time());
        }else {
            $test = explode('-', $_GET['search']);
            $year = $test[1];
            $month = $test[0];
            $start = $year.'-'.$month.'-1';
            $end = $year.'-'.$month.'-'.date("t", strtotime($year.'-'.$month));
        }
        
        $users = Users::find()->where(['users_roles_id' => Users::TYPE_USER])->count();
        $sum = Leads::find()
            ->select('SUM(price) as price')
            ->asArray()
            ->andFilterWhere(['between', 'date',  $start.' 00:00:00', $end.' 00:00:00'])
            ->one();
        $searchModel = new UsersRaitingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $year, $month);

        $years = Leads::find()
            ->select('YEAR(date) as date')
            ->distinct()
            ->orderBy('date DESC')
            ->all();
        
        return $this->render('index', [
            'users' => $users,
            'sum' => $sum,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'start' => $start,
            'end' => $end,
            'year' => $year,
            'month' => $month,
            'years' => $years
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
