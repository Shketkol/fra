$('.js_search_income').on('change', function () {
    serch();
})

$('.js_search_outgo').on('change', function () {
    serch1();
})

$('.js_royalty').on('change', function () {
    serch2();
})

$('.js_index').on('change', function () {
    serch3();
})

function serch() {
    var serch_inp = [];
    $('.js_search_income').each(function (elem, i) {
        $(this).find('option').each(function (elem1, i1) {
            if($(this).is(':selected')){
                serch_inp.push($(this).val());
            }
        })
    });
    var inp = serch_inp.join('-');
    var pattern = /(&search_income=\d+-\d+)|(&search_outgo=\d+-\d+).*/g;
    var url = window.location.search;

    if (window.location.search.search(pattern) < 0){
        window.location.search = url + '&search_income='+inp;
    } else {
        window.location.search = url.replace(pattern, '&search_income='+inp);
    }
}

function serch1() {
    var serch_inp = [];
    $('.js_search_outgo').each(function (elem, i) {
        $(this).find('option').each(function (elem1, i1) {
            if($(this).is(':selected')){
                serch_inp.push($(this).val());
            }
        })
    });
    var inp = serch_inp.join('-');
    var pattern = /(&search_income=\d+-\d+)|(&search_outgo=\d+-\d+).*/g;
    var url = window.location.search;

    if (window.location.search.search(pattern) < 0){
        window.location.search = url + '&search_outgo='+inp;
    } else {
        window.location.search = url.replace(pattern, '&search_outgo='+inp);
    }
}

function serch2() {
    var serch_inp = [];
    $('.js_royalty').each(function (elem, i) {
        $(this).find('option').each(function (elem1, i1) {
            if($(this).is(':selected')){
                serch_inp.push($(this).val());
            }
        })
    });
    var inp = serch_inp.join('-');
    var pattern = /(&search=\d+-\d+)|(&search=\d+-\d+).*/g;
    var url = window.location.search;

    if (window.location.search.search(pattern) < 0){
        window.location.search = url + '&search='+inp;
    } else {
        window.location.search = url.replace(pattern, '&search='+inp);
    }
}

function serch3() {
    var serch_inp = [];
    $('.js_index').each(function (elem, i) {
        $(this).find('option').each(function (elem1, i1) {
            if($(this).is(':selected')){
                serch_inp.push($(this).val());
            }
        })
    });
    var inp = serch_inp.join('-');
    window.location.search = '?search='+inp;
}

$('.js_config').on('click', function () {
    var parent = $(this).parent(),
        id = $(this).data('id'),
        value = parent.find('input').val();
    $.ajax({
        type: "GET",
        url: '/admin/config/default/update',
        data: 'id='+id+'&value='+value,
        success: function(data) {

        }
    });
})
