<?php
return [
    //users
    '/users' => 'users/users/index',
    '/users/create' => 'users/users/create',
    '/users/update' => 'users/users/update',
    '/users/delete' => 'users/users/delete',
    '/users/invite' => 'users/users/invite',

    '/partner/income/<id:[\d]+>' => 'admin/income/income/index',
    'royalty/<if:[\d]+>' => 'royalty/royalty/index',


    '/logout' => 'site/logout',

];
