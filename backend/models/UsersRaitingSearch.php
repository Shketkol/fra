<?php

namespace backend\models;

use common\models\Royalty;
use common\models\UsersRaiting;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Users;
use yii\data\ArrayDataProvider;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class UsersRaitingSearch extends UsersRaiting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'users_roles_id'], 'integer'],
            [['name', 'email', 'created_at', 'updated_at', 'price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $year, $month)
    {
//        $start = '2017-10-01';
//        $end = '2017-10-03';
//        $query = UsersRaiting::find();
        
//        $query = Users::find();
//            ->join('LEFT JOIN', 'leads', 'leads.users_id=users.id')
//            ->andFilterWhere(['between', 'leads.date',  $start.' 00:00:00', $end.' 00:00:00']);
        // add conditions that should always apply here

        $data = array();
        foreach (Users::find()->all() as $value){
            $ar = array(
                'id' => $value->id,
                'name' => $value->name,
                'city' => $value->usersProfile->city,
                'email' => $value->email,
                'royalti' => Royalty::getAllRoyailty($value->id, $year, $month)
            );
            $data[] = $ar;
        }

        
//        $dataProvider = new ActiveDataProvider([
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => [
                    'royalti' => [
                        'asc' => ['royalti' => SORT_ASC, 'royalti' => SORT_ASC],
                        'desc' => ['royalti' => SORT_DESC, 'royalti' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => array(
                    'royalti' => SORT_DESC
                )
            ],
            'pagination' => [ 'pageSize' => 20 ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'status' => $this->status,
//            'users_roles_id' => $this->users_roles_id,
//            'email_confirmed' => $this->email_confirmed,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//        ]);
        
        return $dataProvider;
    }
}
