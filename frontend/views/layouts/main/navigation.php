<?
use \yii\helpers\Url;
?>
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Навигация</span></h3> </div>
        <ul class="nav" id="side-menu">
            <li> <a href="<?=Url::to(['/site/index'])?>" class="waves-effect"><i class="mdi mdi-gauge fa-fw"></i><span class="hide-menu">Главная панель</span></a></li>
            <li><a href="<?=Url::to(['/finance/finance/index'])?>" class="waves-effect"><i class="mdi mdi-currency-rub fa-fw"></i><span class="hide-menu">Финансы</span></a></li>
            <li><a href="<?=Url::to(['/royalty/royalty/index'])?>" class="waves-effect"><i class="mdi mdi-plus fa-fw"></i><span class="hide-menu">Расчет роялти</span></a></li>
            <li><a href="<?=Url::to(['/income/income/index'])?>" class="waves-effect"><i class="mdi mdi-clipboard-flow  fa-fw"></i><span class="hide-menu"> Доход/Расход</span></a> </li>
            <li><a href="<?=Url::to(['/planning/planning/index'])?>" class="waves-effect"><i class="mdi mdi-chart-pie  fa-fw"></i><span class="hide-menu"> Фин. планирование</span></a> </li>
            <li><a href="<?=Url::to(['/users/users/profile'])?>" class="waves-effect"><i class="mdi mdi-face  fa-fw"></i><span class="hide-menu"> Профиль</span></a> </li>
            <li><a href="<?=Url::to(['/auth/users/logout'])?>" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">Выйти</span></a></li>
        </ul>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Left Sidebar -->
<!-- ============================================================== -->