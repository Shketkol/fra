<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AdminMainAsset;
use frontend\assets\AdminHeadAsset;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AdminHeadAsset::register($this);
AdminMainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="myApp">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" type="image/png" sizes="16x16" href="/plugins/images/favicon.png">
</head>
<body ng-cloak ng-controller="mainController">
<?php $this->beginBody() ?>

<?=\frontend\widgets\alert\MessageWidget::widget()?>

<?=$this->render('main/header')?>

<?=$this->render('main/navigation')?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <?if(!\common\models\Users::testprofile()){?>
                    <div id="alerttopright" class="myadmin-alert alert-info myadmin-alert-top-right" style="display: none;"> <a href="#" class="closed">×</a>
                        <h4>У Вас не заполнен профиль</h4>Чтобы пользоваться всеми возможностями заполните профиль</div>

                    <?
                    $script = <<< JS
                   $(document).ready(function() {
                        $("#alerttopright").fadeToggle(350);
                        
                        $(".myadmin-alert .closed").click(function (event) {
                            $(this).parents(".myadmin-alert").fadeToggle(350);
                            return false;
                        });
                        $(".myadmin-alert-click").click(function (event) {
                            $(this).fadeToggle(350);
                            return false;
                        });
                    }); 
JS;
                    $this->registerJs($script, yii\web\View::POS_END);
                    ?>

                <? }?>
                <?=(isset($this->blocks['navBlock']))?$this->blocks['navBlock']:'' ?>
                <?
                echo Breadcrumbs::widget([
                    'options' => ['class' => 'breadcrumb m-r-10'],
                    'homeLink' => ['label' => 'Главная панель', 'url' => ['/']],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]);
                ?>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <?=$content?>

    </div>

    <?=$this->render('main/footer')?>
</div>



<?
$script = <<< JS
(function () {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
            new CBPFWTabs(el);
        });
    })();
// // jQuery(document).ready(function () {
//         $("input[name='tch3']").TouchSpin({
//             min: 1,
//             max: 9999,
//             buttondown_class: 'btn btn-info btn-outline',
//             buttonup_class: 'btn btn-info btn-outline',
//         });

function search_range(start, end) {
    //var pattern = /(&search=\d+-\d+)|(&search=\d+-\d+).*/g;
    //var url = window.location.search;
    //window.location = url + '&start='+start+'&end='+end;
    
    //var pattern_start = /(&start=\d+-\d+)|(&start=\d+-\d+).*/g;
    //var pattern_end = /(&end=\d+-\d+)|(&end=\d+-\d+).*/g;
    
    var urlX = new URL(window.location.href);
    var id = urlX.searchParams.get("id");
    
    window.location = window.location.href.split("?")[0] + '?id=' + id + '&start=' + start + '&end=' + end;
    
    /*if (window.location.search.search(pattern) < 0){
        window.location.search = url + '&start='+start+'&end='+end;
    } else {
        window.location.search = url.replace(pattern, '&search='+inp);
    }*/
}

$('input[name="daterange"]').daterangepicker(
    {
        locale: {
          format: 'YYYY-MM-DD'
        },
        //startDate: '2013-01-01',
        //endDate: '2013-12-31'
    }, 
    function(start, end, label) {
        //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        search_range(parseInt(start/1000), parseInt(end/1000));
    }
);

JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
