<?php
namespace frontend\modules\cron\controllers;

use common\models\Config;
use common\models\Leads;
use common\models\LoginForm;
use common\models\Products;
use common\models\Users;
use common\models\ProductCitiesPercent;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `cron` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $amo_domain = Config::findOne(['key' => 'amo_domain']);
        $amo_login = Config::findOne(['key' => 'amo_login']);
        $amo_hash = Config::findOne(['key' => 'amo_hash']);
        if (!empty($amo_domain) && !empty($amo_login) && !empty($amo_hash)) {
            $this->amo($amo_domain->value, $amo_login->value, $amo_hash->value);
        }

        Yii::$app->end();
        return $this->render('index');
    }

    protected function amo($amo_domain, $amo_login, $amo_hash)
    {
        $amo = new \AmoCRM\Client($amo_domain, $amo_login, $amo_hash);
        $max_days = 40; // 289
        $min_days = 29; // 0
        for($d = $min_days; $d < $max_days; $d++){
            echo '<br>----------<br>DAY NUMBER '.$d.'<br>';
            $offset = 0;
            if($d == 25) $offset = 54500;
            while(true){
                
                echo 'OFFSET: ' . $offset . '<br>';
                
                $leads = $amo->lead->apiList([
                    //'query' => '',
                    'limit_rows' => 500,
                    'limit_offset' => $offset
                ], "-$d DAYS");
                
                //echo count($leads).'<br>';
                
                $offset += 500;
                if(empty($leads)) break;
                foreach ($leads as $lead) {
                    //echo $lead['id'].'<br>';
                    $leadCheck = Leads::findOne(['amo_id' => $lead['id']]);
                    if (!empty($lead['price']) & !$leadCheck) {
                        echo $lead['id'].' - price ok<br>';
                        $is_product = false;
                        $is_date = false;
                        $city = '';
                        $date_real = '';

                        /*
                        8209350 - предоплата
                        8212066 - доплата
                        142 - онлайн оплата
                        143 - закрыто и не реализовано 
                        */

                        $responsible_user = $lead['responsible_user_id'];
                        $status_id = $lead['status_id'];

                        // не оплачен
                        if($status_id != 8209350 & $status_id != 8212066 & $status_id != 142) continue;
                        
                        if($status_id == 8212066 || $status_id == 142) $status = 'paid';
                        else if($status_id == 8209350) $status = 'prepaid';
                        else $status = 'undefined';
                        
                        //echo $lead['id'].' - paid ok, status: '.$status.'<br>';

                        foreach ($lead['custom_fields'] as $field) {
                            // продукт
                            if ($field['id'] == '1740719') {
                                $product_name = $field['values'][0]['value'];
                                $is_product = true;
                            }
                            // дата фактической полной оплаты
                            if ($field['id'] == '1748498') {
                                $date_real = $field['values'][0]['value'];
                                $is_date = true;
                            }
                        }

                        // нет даты оплаты
                        if($is_date == false) continue;

                        // не указан продукт
                        if($is_product == false){
                            $product_name = $lead['name'];
                            //continue;
                        }
                        
                        // поиск пользователя
                        $user = Users::find()
                                ->join('RIGHT JOIN', 'users_profile', 'users_profile.users_id=users.id')
                                ->where(['users_profile.amo_id' => $responsible_user])
                                ->one();

                        // нет франчайзи
                        if(empty($user)) continue;

                        // добавить / загрузить продукт
                        $product = Products::findOne(["name" => $product_name]);
                        if(!$product){
                            $product = new Products();
                            $product->name = $product_name;
                            $product->save();

                            $productCity = new ProductCitiesPercent();
                            $productCity->products_id = $product->id;
                            $productCity->city_people_id = 1;
                            $productCity->value = 0;
                            $productCity->save();
                        }

                        $contact = $amo->contact->apiLinks(['deals_link' => $lead['id']]);
                        if(!isset($contact[0])){
                            $contact = array();
                            $contact[0]['name'] = 'Не задан';
                            $contact[0]['custom_fields'][0]['name'] = 'Телефон';
                            $contact[0]['custom_fields'][0]['values'][0]['value'] = 'Не задан';
                            $contact[0]['custom_fields'][1]['name'] = 'Email';
                            $contact[0]['custom_fields'][1]['values'][0]['value'] = 'Не задан';
                        }
                        else $contact = $amo->contact->apiList(['id' => array($contact[0]['contact_id'])]);


                        echo $lead['name'] . ' - ' . $user->name . ', '. $date_real . '<br>';

                        $leadCheck = Leads::findOne(['amo_id' => $lead['id']]);
                        if($leadCheck){
                            echo 'EXISTS<br>';
                            continue;
                        }

                        if($is_product == true) $isp = 1;
                        else $isp = 0;
                        
                        $lead_db = new Leads();
                        $lead_db->status = $status;
                        $lead_db->name_lead = $lead['name'];
                        $lead_db->users_id = $user->id;
                        $lead_db->products_id = $product->id;
                        $lead_db->price = $lead['price'];
                        $lead_db->amo_id = $lead['id'];
                        $lead_db->is_product = $isp;
                        $lead_db->contact = $contact[0]['name'];
                        foreach ($contact[0]['custom_fields'] as $v) {
                            if ($v['name'] == 'Телефон') {
                                $lead_db->phone = $v['values'][0]['value'];
                            }
                            if ($v['name'] == 'Email') {
                                $lead_db->email = $v['values'][0]['value'];
                            }
                        }
                        $lead_db->type = Leads::TYPE_ONLINE;
                        $lead_db->date = $date_real;
                        $lead_db->save(false);
                    }
                }
            }//offset
        } // for
    }

}
