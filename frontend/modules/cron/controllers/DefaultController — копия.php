<?php

namespace frontend\modules\cron\controllers;

use common\models\Config;
use common\models\Leads;
use common\models\LoginForm;
use common\models\Products;
use common\models\Users;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `cron` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $amo_domain = Config::findOne(['key' => 'amo_domain']);
        $amo_login = Config::findOne(['key' => 'amo_login']);
        $amo_hash = Config::findOne(['key' => 'amo_hash']);
        if (!empty($amo_domain) && !empty($amo_login) && !empty($amo_hash)) {
            $this->amo($amo_domain->value, $amo_login->value, $amo_hash->value);
        }

        Yii::$app->end();
        return $this->render('index');
    }

    protected function amo($amo_domain, $amo_login, $amo_hash)
    {
        $amo = new \AmoCRM\Client($amo_domain, $amo_login, $amo_hash);
        foreach (Products::find()->all() as $product) {
            $leads = $amo->lead->apiList([
                'query' => $product->name,
            ], '-1 DAYS');
            if (!empty($leads)) {
                foreach ($leads as $lead) {
                    if (!empty($lead['price']) && empty(Leads::findOne(['amo_id' => $lead['id']]))) {
                        if (!empty($lead['price'])) {
                            $is_produce = false;
                            $is_franch = false;
                            $city = '';
                            foreach ($lead['custom_fields'] as $field) {
                                if ($field['name'] == 'Продукт' && $field['values'][0]['value'] == $product->name) {
                                    $is_product = true;
                                }
                                if($field['name'] == "Город"){
                                    $city = $field['values'][0]['value'];
                                }
                                if($field['name'] == "Менеджер (Франчайзи)" && $field['values'][0]['value'] == 'Франчайзи'){
                                    $is_franch = true;
                                }
                            }
                            if (empty(Leads::findAll(['amo_id'])) && $is_product == true && $is_franch == true) {
                                $contact = $amo->contact->apiLinks(['deals_link' => $lead['id']]);
                                if(!isset($contact[0])){
                                    continue; // не у всех указаны контактные данные, сделать обработку
                                }
                                $contact = $amo->contact->apiList(['id' => array($contact[0]['contact_id'])]);

                                //echo '<pre>';
                                //var_dump($lead);
                                //echo '</pre>';
                                //continue;
                                
                                $user = Users::find()
                                    ->join('RIGHT JOIN','users_profile', 'users_profile.users_id=users.id')
                                    ->where(['users_profile.city' => $city])
                                    ->one();
                                /*
                                $user = Yii::$app->db->createCommand()
                                    ->select('id')
                                    ->from('users u')
                                    ->join('users_profile up', 'u.id=up.users_id')
                                    ->where(['up.city' => $city])
                                    ->queryRow()-one();
                                */
                                
                                if (!empty($user)) {
                                    
                                    echo $lead['name'] . ' - ' . $user->name . '<br>';
                                    
                                    $lead_db = new Leads();
                                    $lead_db->name_lead = $lead['name'];
                                    $lead_db->users_id = $user->id;
                                    $lead_db->products_id = $product->id;
                                    $lead_db->price = $lead['price'];
                                    $lead_db->amo_id = $lead['id'];
                                    $lead_db->contact = $contact[0]['name'];
                                    foreach ($contact[0]['custom_fields'] as $v) {
                                        if ($v['name'] == 'Телефон') {
                                            $lead_db->phone = $v['values'][0]['value'];
                                        }
                                        if ($v['name'] == 'Email') {
                                            $lead_db->email = $v['values'][0]['value'];
                                        }
                                    }
                                    $lead_db->type = Leads::TYPE_ONLINE;
                                    $lead_db->date = date('Y-m-d H:i:s', $lead['last_modified']);
                                    $lead_db->save(false);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
