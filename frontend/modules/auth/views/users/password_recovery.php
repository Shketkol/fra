<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
?>
<section id="wrapper" class="new-login-register" ng-controller="authController">
    <div class="lg-info-panel">
        <div class="inner-panel">
        </div>
    </div>
    <div class="new-login-box">
        <div class="white-box">
            <h3 class="box-title m-b-0">Восстановление пароля</h3>
                <? $form = ActiveForm::begin([
                    'id' => 'loginform',
                    'enableClientValidation'=> false,
                    'validateOnSubmit' => true,
                    'action' => Url::to(['password-recovery', 'token' => $token]),
                    'options' => [
                        'class' => 'form-horizontal form-material new-lg-form',
                        'name' => 'log',
                        'autocomplete' => 'off',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}<div class=\"col-xs-12\">{input}<span class=\"help-block hidden\"></span></div>",
                    ],
                ]); ?>
            <?=$form->errorSummary($model, ['class'=>'error-summary-no-header']);?>
            <?= $form->field($model, 'password_hash')->textInput(['type' => 'password', 'placeholder' => 'Пароль', 'ng-model' => 'user.password', 'required' => true])->label(false) ?>
            <?= $form->field($model, 'confirm_pass')->textInput(['type' => 'password', 'placeholder' => 'Подтвердите пароль', 'ng-model' => 'user.confirm_password', 'required' => true])->label(false) ?>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button ng-click="auth($event, log, user)" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                                type="submit"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Отправить &nbsp;</button>
                    </div>
                </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>
</section>

<script>
    var password = document.getElementById("users-password_hash")
        , confirm_password = document.getElementById("users-confirm_pass");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Пароли не совпадают");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>