<?php

namespace frontend\modules\income\controllers;

use common\models\IncomeUsers;
use common\models\Leads;
use common\models\Outgo;
use common\models\OutgoUsers;
use common\models\Products;
use frontend\models\UsersSearch;
use common\models\Customers;
use common\models\Users;
use common\models\UsersProfile;
use frontend\components\controllers\DefaultController;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\User;

/**
 * Default controller for the `users` module
 */
class IncomeController extends DefaultController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $active = true;
        if(!isset($_GET['search_income'])){
            $income = Leads::find()
                ->where(['users_id' => Yii::$app->user->id])
                ->orderBy('date DESC')
                ->one();
            if(isset($income->date)){
                $month = date('m', strtotime($income->date));
                $year = date('Y', strtotime($income->date));
            }
            else{
                $month = date('m');
                $year = date('Y');
            }
        }else {
            $test = explode('-', $_GET['search_income']);
            $month = $test[0];
            $year = $test[1];
            $active = true;
        }
        $income_all = [];
        foreach (Products::find()->all() as $item) {
            $income = [
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between',"date",$year.'-'.$month.'-1 00:00:00',$year.'-'.$month.'-5 23:59:59'])
                    ->andWhere(['products_id' => $item->id])
                    ->andWhere(['users_id' => Yii::$app->user->id])
                    ->asArray()
                    ->one(),
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between',"date",$year.'-'.$month.'-6 00:00:00',$year.'-'.$month.'-11 23:59:59'])
                    ->andWhere(['products_id' => $item->id])
                    ->andWhere(['users_id' => Yii::$app->user->id])
                    ->asArray()
                    ->one(),
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between',"date",$year.'-'.$month.'-12 00:00:00',$year.'-'.$month.'-18 23:59:59'])
                    ->andWhere(['products_id' => $item->id])
                    ->andWhere(['users_id' => Yii::$app->user->id])
                    ->asArray()
                    ->one(),
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between',"date",$year.'-'.$month.'-19 00:00:00',$year.'-'.$month.'-25 23:59:59'])
                    ->andWhere(['products_id' => $item->id])
                    ->andWhere(['users_id' => Yii::$app->user->id])
                    ->asArray()
                    ->one(),
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between',"date",$year.'-'.$month.'-26 00:00:00',$year.'-'.$month.'-31 23:59:59'])
                    ->andWhere(['products_id' => $item->id])
                    ->andWhere(['users_id' => Yii::$app->user->id])
                    ->asArray()
                    ->one(),
            ];
            $income_all[$item->name] = $income;
        }

        $years_income = IncomeUsers::find()
            ->select('year')
            ->distinct()
            ->where(['users_id' => Yii::$app->user->id])
            ->orderBy('year DESC')
            ->all();
        $years_outgo = IncomeUsers::find()
            ->select('year')
            ->distinct()
            ->where(['users_id' => Yii::$app->user->id])
            ->orderBy('year DESC')
            ->all();


        $products = Outgo::find()
            ->where(['users_id' => Yii::$app->user->id])
            ->all();

        return $this->render('index', [
            'income' => $income_all,
            'month' => $month,
            'year' => $year,
            'years_income' => $years_income,
            'years_outgo' => $years_outgo,
            'active' => $active,
            'products' => $products,
            'm' => $month,
            'y' => $year
        ]);
    }

    public function actionIncomeCreate(){
        $model = new IncomeUsers();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
        return $this->render('income-create', [
            'model' => $model,
        ]);
    }

    public function actionIncomeUpdate($id){
        $model = IncomeUsers::findOne(['id' => $id]);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
        return $this->render('income-create', [
            'model' => $model,
        ]);
    }

    public function actionOutgoCreate(){
        $model = new OutgoUsers();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
        return $this->render('outgo-create', [
            'model' => $model,
        ]);
    }

    public function actionOutgoUpdate($id){
        $model = OutgoUsers::findOne(['id' => $id]);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
        return $this->render('outgo-create', [
            'model' => $model,
        ]);
    }

}


