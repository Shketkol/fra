<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = 'Доход/Расход';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>
<div class="row">
    <div class="col-md-12">
        <? $form = ActiveForm::begin([
            'id' => 'users-update',
            'enableClientValidation'=> false,
            'validateOnSubmit' => true,
            'options' => [
                'class' => 'form-horizontal',
                'autocomplete' => 'off',
            ],
            'fieldConfig' => [
                'template' => "{label}<div class=\"col-md-12\">{input}<span class=\"help-block hidden\"></span></div>",
                'labelOptions' => ['class' => 'col-md-12'],
            ],
        ]); ?>
        <div class="white-box">
            <?=$form->errorSummary($model);?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'week')->dropDownList(
                        array(
                            1 => '1 неделя',
                            2 => '2 неделя',
                            3 => '3 неделя',
                            4 => '4 неделя',
                            5 => '5 неделя',
                        ),
                        array('prompt' => 'Выберите значение')
                    ) ?>
                    <?= $form->field($model, 'income_field')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\common\models\IncomeField::find()->where(['users_id' => Yii::$app->user->id])->all(), 'id', 'name'),
                        array('prompt' => 'Выберите значение')
                    ) ?>
                    <?= $form->field($model, 'users_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
                    <?= $form->field($model, 'value')->textInput(['required' => true]) ?>
                    <?= $form->field($model, 'month')->dropDownList(
                        array(
                            1 => 'январь',
                            2 => 'февраль',
                            3 => 'март',
                            4 => 'апрель',
                            5 => 'май',
                            6 => 'июль',
                            7 => 'июнь',
                            8 => 'август',
                            9 => 'сентябрь',
                            10 => 'ноябрь',
                            11 => 'октябрь',
                            12 => 'декабрь',
                        ),
                        array('prompt' => 'Выберите значение')
                    ) ?>
                    <?= $form->field($model, 'year')->textInput(['required' => true]) ?>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>

        <? ActiveForm::end(); ?>
    </div>
</div>