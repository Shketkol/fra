<?php

namespace frontend\modules\royalty\controllers;

use common\models\Leads;
use common\models\Products;
use common\models\Royalty;
use common\models\Users;
use frontend\components\controllers\DefaultController;
use yii\filters\AccessControl;
use Yii;

class RoyaltyController extends DefaultController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    public function afterAction($action, $result)
    {
        Yii::$app->getUser()->setReturnUrl(Yii::$app->request->url);
        return parent::afterAction($action, $result);
    }


    public function actionIndex()
    {
        if (!isset($_GET['search'])) {
            $year = date('Y', time());
            $month = date('m', time());
        } else {
            $test = explode('-', $_GET['search']);
            $year = $test[1];
            $month = $test[0];
        }

        $royalty = 0;
        foreach (Products::find()->all() as $value) {
            $royalty = $royalty + Royalty::getRoyaltyForUser(Yii::$app->user->id, $value, $year, $month);
        }

        $years = Leads::find()
            ->select('YEAR(date) as date')
            ->distinct()
            ->where(['users_id' => Yii::$app->user->id])
            ->orderBy('date DESC')
            ->all();

        $show = true;
        $yesterday = Royalty::find()
            ->where(['users_id' => Yii::$app->user->id])
            ->andFilterWhere(['like', 'created_at', $year . '-' . $month])
            ->one();
        if (!empty($yesterday)) {
            $show = false;
        }

        return $this->render('index', [
            'royalti' => $royalty,
            'month' => $month,
            'year' => $year,
            'years' => $years,
            'show' => $show
        ]);
    }

    public function actionRoyalty()
    {
        $user = Users::findOne(['id' => Yii::$app->user->id]);
        if (Yii::$app->request->post()) {
            $royalty = new Royalty();
            $royalty->load(Yii::$app->request->post());
            $royalty->users_id = $user->id;
            $royalty->agree = $_POST['Royalti']['agree'];
            $royalty->value = $_POST['Royalti']['value'];
            $royalty->created_at = $_POST['Royalti']['year'] . '-' . $_POST['Royalti']['month'];
            $royalty->comment = isset($_POST['Royalti']['comment']) ? $_POST['Royalti']['comment'] : null;
            $royalty->save();
            return $this->goBack();
        }

    }
}
