<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Расчет роялти';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="white-box">
                    <div role="tabpanel" class="tab-pane" id="home">
                        <div class="col-md-2 pull-right">
                            <div class="form-group">
                                <select class="form-control js_royalty">
                                    <option value="01" <?=($month == '01') ? 'selected' : null?>>январь</option>
                                    <option value="02" <?=($month == '02') ? 'selected' : null?>>февраль</option>
                                    <option value="03" <?=($month == '03') ? 'selected' : null?>>март</option>
                                    <option value="04" <?=($month == '04') ? 'selected' : null?>>апрель</option>
                                    <option value="05" <?=($month == '05') ? 'selected' : null?>>май</option>
                                    <option value="06" <?=($month == '06') ? 'selected' : null?>>июнь</option>
                                    <option value="07" <?=($month == '07') ? 'selected' : null?>>июль</option>
                                    <option value="08" <?=($month == '08') ? 'selected' : null?>>август</option>
                                    <option value="09" <?=($month == '09') ? 'selected' : null?>>сентябрь</option>
                                    <option value="10"<?=($month == 10) ? 'selected' : null?>>октябрь</option>
                                    <option value="11" <?=($month == 11) ? 'selected' : null?>>ноябрь</option>
                                    <option value="12" <?=($month == 12) ? 'selected' : null?>>декабрь</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 pull-right">
                            <div class="form-group">
                                <select class="form-control js_royalty">
                                    <?if(!empty($years)){?>
                                    <?foreach ($years as $value){?>
                                        <option value="<?=date('Y', strtotime($value->date))?>" <?=($year == date('Y', strtotime($value->date))) ? 'selected' : null?>><?=date('Y', strtotime($value->date))?></option>
                                    <? }?>
                                    <? } else {?>
                                        <option value="<?=date('Y', time())?>" <?=($year == date('Y', time())) ? 'selected' : null?>><?=date('Y', time())?></option>
                                    <? }?>
                                </select>
                            </div>
                        </div>
                    <div class="text-left pull-left" style="margin-top: -10px;">
                        <h1>Роялти, итого к выплате:</h1>
                    </div>
                    <div class="text-right">
                        <h1><?=($royalti > 0) ? '+'.$royalti : $royalti?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="col-md-12">
            <div class="panel white-box">
                <table class="table table-hover manage-u-table table-responsive">
                    <tr>
                        <th>Продукт</th>
                        <th>Сумма</th>
                        <th>Количество сделок</th>
                        <th>Средний чек</th>
                        <th>Процент по роялти</th>
                    </tr>
                    <?foreach (\common\models\Products::find()->all() as $value){ ?>
                        <tr>
                            <th><a href="<?=Url::to(['/finance/finance/product', 'id' => $value->id])?>"><?=$value->name?></a></th>
                            <th><?=\common\models\Royalty::getRoyaltyForUser(Yii::$app->user->id, $value, $year, $month)?></th>
                            <th><?=common\models\Royalty::getAllLeads(Yii::$app->user->id, $value, $year, $month)?></th>
                            <?if(common\models\Royalty::getAllLeads(Yii::$app->user->id, $value, $year, $month) > 0){?>
                                <th><?=number_format(\common\models\Royalty::getRoyaltyForUser(Yii::$app->user->id, $value, $year, $month) / \common\models\Royalty::getAllLeads(Yii::$app->user->id, $value, $year, $month), 2, ".", '')?></th>
                            <? } else {?>
                                <th>0</th>
                            <? } ?>
                            <th><?=\common\models\ProductCitiesPercent::getPercent($value, Yii::$app->user->id)?></th>
                        </tr>
                    <? } ?>
                </table>
            </div>
        </div>
        <? if($show){?>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="panel">
                        <? $form = \yii\bootstrap\ActiveForm::begin([
                            'enableClientValidation' => false,
                            'validateOnSubmit' => true,
                            'action' => Url::to(['/royalty/royalty/royalty'])
                        ]); ?>
                        <input type="hidden" name="Royalti[agree]" value="0">
                        <input type="hidden" name="Royalti[value]" value="<?=$royalti?>">
                        <input type="hidden" name="Royalti[year]" value="<?=$year?>">
                        <input type="hidden" name="Royalti[month]" value="<?=$month?>">
                        <textarea class="form-control" name="Royalti[comment]" rows="5" placeholder="Текст коментария"></textarea>
                        <button class="btn btn-block btn-danger">Не согласен</button>
                        <? \yii\bootstrap\ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel">
                        <? $form = \yii\bootstrap\ActiveForm::begin([
                            'enableClientValidation' => false,
                            'validateOnSubmit' => true,
                            'action' => Url::to(['/royalty/royalty/royalty'])
                        ]); ?>
                        <input type="hidden" name="Royalti[agree]" value="1">
                        <input type="hidden" name="Royalti[value]" value="<?=$royalti?>">
                        <input type="hidden" name="Royalti[year]" value="<?=$year?>">
                        <input type="hidden" name="Royalti[month]" value="<?=$month?>">
                        <button class="btn btn-block btn-success">Согласен</button>
                        <? \yii\bootstrap\ActiveForm::end(); ?>
                    </div>
                </div>
            </div>

        </div>
        <? } ?>
</div>

