<?php

namespace frontend\modules\finance\controllers;

use common\models\Config;
use common\models\Leads;
use common\models\LeadsSearch;
use common\models\Products;
use common\models\Users;
use common\models\UsersRaiting;
use frontend\components\controllers\DefaultController;
use yii\filters\AccessControl;
use Yii;

/**
 * Finance controller for the `finance` module
 */
class FinanceController extends DefaultController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        
        if(!isset($_GET['start'])){
            $start = date("Y-m-1", time());
        }
        else{
            $start = date("Y-m-d", $_GET['start']);
        }
        
        if(!isset($_GET['end'])){
            $end = date("Y-m-t", time());
        }
        else{
            $end = date("Y-m-d", $_GET['end']);
        }
        
        if(!isset($_GET['search'])){
            $year = date('Y', time());
            $month = date('m', time());  
        }else {
            $test = explode('-', $_GET['search']);
            $year = $test[1];
            $month = $test[0];
        }
        
        $all_online = Leads::find()
            ->select('SUM(price) as value')
            ->where(['users_id' => Yii::$app->user->id, 'type' => Leads::TYPE_ONLINE])
            ->andFilterWhere(['between', 'date',  $start, $end])
            ->asArray()
            ->one();
        $all_online = (empty($all_online['value'])) ? '0.00' : $all_online['value'];

        $all_offline = Leads::find()
            ->select('SUM(price) as value')
            ->where(['users_id' => Yii::$app->user->id, 'type' => Leads::TYPE_OFFLINE])
            ->andFilterWhere(['between', 'date',  $start, $end])
            ->asArray()
            ->one();
        $all_offline = (empty($all_offline['value'])) ? '0.00' : $all_offline['value'];


        return $this->render('index', [
            'all_online' => $all_online,
            'all_offline' => $all_offline,
            'start' => $start,
            'end' => $end
        ]);
    }

    public function actionView($id, $type)
    {
        $searchModel = new LeadsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->id, $id, $type);
        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionProduct($id)
    {
        $searchModel = new LeadsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->id, $id);
        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Leads();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->products_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionNew()
    {
        $amo_domain = Config::findOne(['key' => 'amo_domain']);
        $amo_login = Config::findOne(['key' => 'amo_login']);
        $amo_hash = Config::findOne(['key' => 'amo_hash']);
        if(!empty($amo_domain) && !empty($amo_login) && !empty($amo_hash)){
            $this->amo($amo_domain->value, $amo_login->value, $amo_hash->value);
        }
        return $this->redirect(['index']);
    }

    protected function amo($amo_domain, $amo_login, $amo_hash)
    {
        $amo = new \AmoCRM\Client($amo_domain, $amo_login, $amo_hash);
            $offset = 0;
            //while(true){
                
                //echo 'OFFSET: ' . $offset . '<br>';
                
                $leads = $amo->lead->apiList([
                    //'query' => '',
                    'limit_rows' => 500,
                    'limit_offset' => $offset
                ], date('D, d M Y 00:00:01', time()));
                
                //echo count($leads).'<br>';
                
                $offset += 500;
                foreach ($leads as $lead) {
                    //echo $lead['id'].'<br>';
                    $leadCheck = Leads::findOne(['amo_id' => $lead['id']]);
                    if (!empty($lead['price']) & !$leadCheck) {
                        //echo $lead['id'].' - price ok<br>';
                        $is_product = false;
                        $is_date = false;
                        $city = '';
                        $date_real = '';

                        /*
                        8209350 - предоплата
                        8212066 - доплата
                        142 - онлайн оплата
                        143 - закрыто и не реализовано 
                        */

                        $responsible_user = $lead['responsible_user_id'];
                        $status_id = $lead['status_id'];

                        // не оплачен
                        if($status_id != 8209350 & $status_id != 8212066 & $status_id != 142) continue;
                        
                        if($status_id == 8212066 || $status_id == 142) $status = 'paid';
                        else if($status_id == 8209350) $status = 'prepaid';
                        else $status = 'undefined';
                        
                        //echo $lead['id'].' - paid ok, status: '.$status.'<br>';

                        foreach ($lead['custom_fields'] as $field) {
                            // продукт
                            if ($field['id'] == '1740719') {
                                $product_name = $field['values'][0]['value'];
                                $is_product = true;
                            }
                            // дата фактической полной оплаты
                            if ($field['id'] == '1748498') {
                                $date_real = $field['values'][0]['value'];
                                $is_date = true;
                            }
                        }

                        // нет даты оплаты
                        if($is_date == false) continue;

                        // не указан продукт
                        if($is_product == false){
                            $product_name = $lead['name'];
                            //continue;
                        }
                        
                        // поиск пользователя
                        $user = Users::find()
                                ->join('RIGHT JOIN', 'users_profile', 'users_profile.users_id=users.id')
                                ->where(['users_profile.amo_id' => $responsible_user, 'users_profile.users_id' => Yii::$app->user->id])
                                ->one();

                        // нет франчайзи
                        if(empty($user)) continue;

                        // добавить / загрузить продукт
                        $product = Products::findOne(["name" => $product_name]);
                        if(!$product){
                            $product = new Products();
                            $product->name = $product_name;
                            $product->save();

                            $productCity = new ProductCitiesPercent();
                            $productCity->products_id = $product->id;
                            $productCity->city_people_id = 1;
                            $productCity->value = 0;
                            $productCity->save();
                        }

                        $contact = $amo->contact->apiLinks(['deals_link' => $lead['id']]);
                        if(!isset($contact[0])){
                            $contact = array();
                            $contact[0]['name'] = 'Не задан';
                            $contact[0]['custom_fields'][0]['name'] = 'Телефон';
                            $contact[0]['custom_fields'][0]['values'][0]['value'] = 'Не задан';
                            $contact[0]['custom_fields'][1]['name'] = 'Email';
                            $contact[0]['custom_fields'][1]['values'][0]['value'] = 'Не задан';
                        }
                        else $contact = $amo->contact->apiList(['id' => array($contact[0]['contact_id'])]);


                        //echo $lead['name'] . ' - ' . $user->name . ', '. $date_real . '<br>';

                        $leadCheck = Leads::findOne(['amo_id' => $lead['id']]);
                        if($leadCheck){
                            //echo 'EXISTS<br>';
                            continue;
                        }

                        if($is_product == true) $isp = 1;
                        else $isp = 0;
                        
                        $lead_db = new Leads();
                        $lead_db->status = $status;
                        $lead_db->name_lead = $lead['name'];
                        $lead_db->users_id = $user->id;
                        $lead_db->products_id = $product->id;
                        $lead_db->price = $lead['price'];
                        $lead_db->amo_id = $lead['id'];
                        $lead_db->is_product = $isp;
                        $lead_db->contact = $contact[0]['name'];
                        foreach ($contact[0]['custom_fields'] as $v) {
                            if ($v['name'] == 'Телефон') {
                                $lead_db->phone = $v['values'][0]['value'];
                            }
                            if ($v['name'] == 'Email') {
                                $lead_db->email = $v['values'][0]['value'];
                            }
                        }
                        $lead_db->type = Leads::TYPE_ONLINE;
                        $lead_db->date = $date_real;
                        $lead_db->save(false);
                    }
                }
            //}//offset
    }
    
    /*
    protected function amo($amo_domain, $amo_login, $amo_hash)
    {
        $amo = new \AmoCRM\Client($amo_domain, $amo_login, $amo_hash);
        foreach (Products::find()->all() as $product) {
            $leads = $amo->lead->apiList([
                'query' => $product->name,
            ], date('D, d M Y 00:00:01', time()));
            if (!empty($leads)) {
                foreach ($leads as $lead) {
                    if (!empty($lead['price']) && empty(Leads::findOne(['amo_id' => $lead['id']]))) {
                        if (!empty($lead['price'])) {
                            $is_produce = false;
                            $is_franch = false;
                            $city = '';
                            foreach ($lead['custom_fields'] as $field) {
                                if ($field['name'] == 'РџСЂРѕРґСѓРєС‚' && $field['values'][0]['value'] == $product->name) {
                                    $is_product = true;
                                }
                                if($field['name'] == "Р“РѕСЂРѕРґ"){
                                    $city = $field['values'][0]['value'];
                                }
                                if($field['name'] == "РњРµРЅРµРґР¶РµСЂ (Р¤СЂР°РЅС‡Р°Р№Р·Рё)" && $field['values'][0]['value'] == 'Р¤СЂР°РЅС‡Р°Р№Р·Рё'){
                                    $is_franch = true;
                                }
                            }
                            if (empty(Leads::findAll(['amo_id'])) && $is_product == true && $is_franch == true) {
                                $contact = $amo->contact->apiLinks(['deals_link' => $lead['id']]);
                                if(!isset($contact[0])){
                                    continue; // РЅРµ Сѓ РІСЃРµС… СѓРєР°Р·Р°РЅС‹ РєРѕРЅС‚Р°РєС‚РЅС‹Рµ РґР°РЅРЅС‹Рµ, СЃРґРµР»Р°С‚СЊ РѕР±СЂР°Р±РѕС‚РєСѓ
                                }
                                $contact = $amo->contact->apiList(['id' => array($contact[0]['contact_id'])]);

                                //echo '<pre>';
                                //var_dump($lead);
                                //echo '</pre>';
                                //continue;

                                $user = Users::find()
                                    ->join('RIGHT JOIN','users_profile', 'users_profile.users_id=users.id')
                                    ->where(['users_profile.city' => $city, 'users_profile.users_id' => Yii::$app->user->id])
                                    ->one();
                                if (!empty($user)) {
                                    $lead_db = new Leads();
                                    $lead_db->name_lead = $lead['name'];
                                    $lead_db->users_id = $user->id;
                                    $lead_db->products_id = $product->id;
                                    $lead_db->price = $lead['price'];
                                    $lead_db->amo_id = $lead['id'];
                                    $lead_db->contact = $contact[0]['name'];
                                    foreach ($contact[0]['custom_fields'] as $v) {
                                        if ($v['name'] == 'РўРµР»РµС„РѕРЅ') {
                                            $lead_db->phone = $v['values'][0]['value'];
                                        }
                                        if ($v['name'] == 'Email') {
                                            $lead_db->email = $v['values'][0]['value'];
                                        }
                                    }
                                    $lead_db->type = Leads::TYPE_ONLINE;
                                    $lead_db->date = date('Y-m-d H:i:s', $lead['last_modified']);
                                    $lead_db->save(false);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    */
}