<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Финансы';
$this->params['breadcrumbs'][] = $this->title;
?>


        <div class="row">
        <div class="col-md-12">
            <div class="panel wallet-widgets">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 80px;']
                        ],
                        [
                            'attribute' => 'amo_id',
                            'format' => 'raw',
                            'value' => function($data){
                                return '<a href="https://'.\common\models\Config::findOne(['key' => 'amo_domain'])->value.'.amocrm.ru/leads/detail/'.$data->amo_id.'" target="_blank">'.$data->amo_id.'</a>';
                            }
                        ],
                        'name_lead',
                        'contact',
                        'state',
                        'price',
                        'email',
                        'phone',
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>

