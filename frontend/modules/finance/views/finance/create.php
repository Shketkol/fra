<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = 'Добавление сделки';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->registerCssFile('@web/public/lib/datetimepicker-master/jquery.datetimepicker.css', ['position' => \yii\web\View::POS_BEGIN]); ?>
<?php $this->registerJsFile('@web/public/lib/datetimepicker-master/jquery.datetimepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<div class="row">
    <div class="col-md-12">
        <? $form = ActiveForm::begin([
            'id' => 'users-update',
            'enableClientValidation'=> false,
            'validateOnSubmit' => true,
            'action' => Url::to(['create']),
            'options' => [
                'class' => 'form-horizontal',
                'autocomplete' => 'off',
            ],
            'fieldConfig' => [
                'template' => "{label}<div class=\"col-md-12\">{input}<span class=\"help-block hidden\"></span></div>",
                'labelOptions' => ['class' => 'col-md-12'],
            ],
        ]); ?>
        <div class="white-box">
            <?=$form->errorSummary($model);?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'type')->hiddenInput(['value' => \common\models\Leads::TYPE_OFFLINE])->label(false) ?>
                    <?= $form->field($model, 'users_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
                    <?= $form->field($model, 'products_id')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\common\models\Products::find()->orderBy('name')->all(), 'id', 'name'),
                        array('prompt' => 'Выберите значение')
                    ) ?>
                    <?/*= $form->field($model, 'form_lead')->dropDownList(\common\models\Leads::$types,
                        array('prompt' => 'Выберите значение')
                    ) */?>
                    <?= $form->field($model, 'date')->textInput(['class' => 'form-control datetimepicker']) ?>
                    <?= $form->field($model, 'users_is')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
                    <?= $form->field($model, 'name_lead')->textInput() ?>
<!--                    --><?//= $form->field($model, 'state')->textInput() ?>
                    <?= $form->field($model, 'contact')->textInput() ?>
                    <?= $form->field($model, 'email')->textInput() ?>
                    <?= $form->field($model, 'phone')->textInput() ?>
                    <?= $form->field($model, 'price')->textInput() ?>
                    <?= $form->field($model, 'amo_id')->textInput() ?>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>

        <? ActiveForm::end(); ?>
    </div>
</div>


<?
$script = <<< JS
   $(document).ready(function() {
        $('.datetimepicker').datetimepicker({
            format: 'Y-m-d',
            lang: 'ru',
            timepicker: false
        });
    }); 
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>