<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Финансы';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php //$this->registerJsFile('plugins/bower_components/jquery/dist/jquery.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel wallet-widgets">
                <div class="panel-body">
                   <div class="row">
                       <div class="col-md-6">
                        <input type="text" value="<?=$start?> - <?=$end?>" name="daterange" class="form-control" />
                        <!--<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>-->
                        </div>
                    </div>
                   
                    <div class="text-left pull-left">
                        <h3>Безналичная выручка:</h3>
                    </div>
                    <div class="text-right">
                        <h3><?=$all_online?></h3>
                    </div>
                </div>
                <ul class="wallet-list">
                    <?foreach (\common\models\Products::find()->all() as $value){ ?>
                        <li><a href="<?=Url::to(['/finance/finance/view', 'id' => $value->id, 'type' => \common\models\Leads::TYPE_ONLINE])?>">
                                <div class="text-left pull-left">
                                    <?=$value->name?>:
                                </div>
                                <div class="text-right">
                                    <?=\common\models\Leads::getSumProductUserOnline(Yii::$app->user->id, $value->id, false, false, $start, $end)?>
                                </div>
                            </a>
                        </li>
                    <? } ?>
                </ul>
                <?if(\common\models\Users::testprofile()){?>
                    <a href="<?=Url::to(['/finance/finance/new'])?>" class="btn btn-block btn-info">ОБНОВИТЬ</a>
                <? } ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel wallet-widgets">
                <div class="panel-body">
                    <div class="text-left pull-left">
                        <h1>Наличная выручка:</h1>
                    </div>
                    <div class="text-right">
                        <h1><?=$all_offline?></h1>
                    </div>
                </div>
                <ul class="wallet-list">
                    <?foreach (\common\models\Products::find()->all() as $value){ ?>
                        <li><a href="<?=Url::to(['/finance/finance/view', 'id' => $value->id, 'type' => \common\models\Leads::TYPE_OFFLINE])?>">
                                <div class="text-left pull-left">
                                    <?=$value->name?>:
                                </div>
                                <div class="text-right">
                                    <?=\common\models\Leads::getSumProductUserOffline(Yii::$app->user->id, $value->id,false,false, $start, $end)?>
                                </div>
                            </a>
                        </li>
                    <? } ?>
                </ul>
                <? echo Html::a('Добавить сделку ', Url::to(['create']), [
                    'class' => 'btn btn-block btn-success'
                ]) ?>
            </div>
        </div>
    </div>
</div>
