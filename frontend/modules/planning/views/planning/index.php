<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Фин. планирование';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-edit m-r-5"></i> Добавить планирование ', Url::to(['create']), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="white-box">
                <div class="text-center">
                    <h1>Планирование</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box table-responsive">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <?foreach ($models as $key=>$model){?>
                    <li role="presentation" class="<?=($key == 0) ? 'active' : null?>"><a href="#planning_<?=$key?>" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> <?=$model->name?></span></a></li>
                <? }?>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <?foreach ($models as $key=>$model){?>
                    <div class="col-md-2 pull-left">
                        <? echo Html::a('<i class="fa fa-edit m-r-5"></i> Редактировать раздел ', Url::to(['update', 'id' => $model->id]), [
                            'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
                        ]) ?>
                    </div>
                    <div role="tabpanel" class="tab-pane <?=($key == 0) ? 'fade active in' : null?>" id="planning_<?=$key?>">
                        <table class="table table-hover manage-u-table">
                            <thead>
                            <tr>
                                <th>название поля</th>
                                <th>январь</th>
                                <th>январь,%</th>
                                <th>февраль</th>
                                <th>февраль,%</th>
                                <th>март</th>
                                <th>март,%</th>
                                <th>апрель</th>
                                <th>апрель,%</th>
                                <th>май</th>
                                <th>май,%</th>
                                <th>июнь</th>
                                <th>июнь,%</th>
                                <th>июль</th>
                                <th>июль,%</th>
                                <th>август</th>
                                <th>август,%</th>
                                <th>сентябрь</th>
                                <th>сентябрь,%</th>
                                <th>октябрь</th>
                                <th>октябрь,%</th>
                                <th>ноябрь</th>
                                <th>ноябрь,%</th>
                                <th>декабрь</th>
                                <th>декабрь,%</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?foreach ($model->content as $val){?>
                                <tr>
                                    <td><?=$val->name_roz?></td>
                                    <td><?=$val->jan?></td>
                                    <td><?=$val->jan_proz?></td>
                                    <td><?=$val->feb?></td>
                                    <td><?=$val->feb_proz?></td>
                                    <td><?=$val->march?></td>
                                    <td><?=$val->march_proz?></td>
                                    <td><?=$val->april?></td>
                                    <td><?=$val->april_proz?></td>
                                    <td><?=$val->may?></td>
                                    <td><?=$val->may_proz?></td>
                                    <td><?=$val->june?></td>
                                    <td><?=$val->june_proz?></td>
                                    <td><?=$val->jule?></td>
                                    <td><?=$val->jule_proz?></td>
                                    <td><?=$val->augest?></td>
                                    <td><?=$val->augest_proz?></td>
                                    <td><?=$val->sept?></td>
                                    <td><?=$val->sept_proz?></td>
                                    <td><?=$val->okt?></td>
                                    <td><?=$val->okt_proz?></td>
                                    <td><?=$val->nov?></td>
                                    <td><?=$val->nov_proz?></td>
                                    <td><?=$val->dec?></td>
                                    <td><?=$val->dec_proz?></td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                <? }?>
            </div>
        </div>
    </div>
</div>