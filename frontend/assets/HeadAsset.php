<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class HeadAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css',
        'public/bootstrap/dist/css/bootstrap.min.css',
        'public/css/animate.css',
        'public/css/style.css',
        'public/css/colors/default.css',
        'public/css/main.css',
    ];
    public $js = [
        //'plugins/bower_components/jquery/dist/jquery.min.js',
        'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js',
        'https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js',
        'http://cdn.jsdelivr.net/momentjs/latest/moment.min.js',
        'http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
    public $cssOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}
