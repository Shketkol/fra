<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css'
    ];
    public $js = [
        //'plugins/bower_components/jquery/dist/jquery.min.js',
        'public/bootstrap/dist/js/bootstrap.min.js',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
        'public/js/jquery.slimscroll.js',
        'public/js/waves.js',
        'public/js/custom.min.js',
        'plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
        'http://cdn.jsdelivr.net/momentjs/latest/moment.min.js',
        'http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
