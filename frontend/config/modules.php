<?php
return [
    'auth' => [
        'class' => 'frontend\modules\auth\Module',
    ],
    'users' => [
        'class' => 'frontend\modules\users\Module',
    ],
    'income' => [
        'class' => 'frontend\modules\income\Module',
    ],
    'finance' => [
        'class' => 'frontend\modules\finance\Module',
    ],
    'planning' => [
        'class' => 'frontend\modules\planning\Module',
    ],
    'cron' => [
        'class' => 'frontend\modules\cron\Module',
    ],
    'royalty' => [
        'class' => 'frontend\modules\royalty\Module',
    ],

];