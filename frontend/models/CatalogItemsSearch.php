<?php

namespace frontend\models;

use common\models\CatalogGroups;
use common\models\CatalogItems;
use common\models\PropertiesCatalogItems;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CatalogItemsSearch represents the model behind the search form about `common\models\CatalogItems`.
 */
class CatalogItemsSearch extends CatalogItems
{
    public $id_sections = array();
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'catalog_groups_id', 'code', 'status'], 'integer'],
            [['name', 'desc', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogItems::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if(!empty($params['name'])){
            $this->name = $params['name'];
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(isset($params['id'])){
            $data = CatalogGroups::find()->asArray()->all();
            $this->id_sections[] = $params['id'];
            $query->andFilterWhere(['in', 'catalog_groups_id', $this->buildTreeFilter($data, $params['id'])]);
        }

        if(isset($params['filter'])){
            $optionsR = $this->parseFilters($params['filter']);
            $goodsIds = array();
            $optionsRIds = array();
            if ($optionsR) {
                $resArray = array();
                $goodsIds = array();
                foreach ($optionsR as $key => $option) {
                    $optionsRIds = array_merge($optionsRIds, $option);

                    $values = PropertiesCatalogItems::find()
                        ->select('catalog_items_id')
                        ->distinct()
                        ->where(['in', 'catalog_items_id', $option])
                        ->all();

                    foreach ($values as $valModel) {
                        $resArray[$key][] = $valModel->catalog_items_id;
                    }
                }
                if (count($optionsR) == count($resArray)) {
                    $n = count($resArray);
                    for ($i=1; $i<$n; $i++) {
                        $resArray[0] = array_intersect($resArray[0], $resArray[$i]);
                    }
                    $goodsIds = $resArray[0];
                }
            }

            $query->andFilterWhere(['in', 'id', $goodsIds]);
        }

            $query->andFilterWhere([
                'code' => $this->code,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'status' => $this->status,
            ]);

            $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'desc', $this->desc]);



        return $dataProvider;
    }


    protected function buildTreeFilter($data, $rootID) {
        foreach ($data as $id => $node) {
            if ($node['parent_id'] == $rootID) {
                unset($data[$id]);
                $node['nodes'] = $this->buildTree($data, $node['id']);
                $this->id_sections[] = $node['id'];
            }
        }
        return $this->id_sections;
    }

    protected function parseFilters($filter = '')
    {
        if (is_array($filter)) {
            return $filter;
        }
        $filterGroups = preg_split('/[_]/', $filter, -1, PREG_SPLIT_NO_EMPTY);
        foreach ($filterGroups as &$filterGroup) {
            $filterGroup = preg_split('/[-]/', $filterGroup, -1, PREG_SPLIT_NO_EMPTY);
        }
        return $filterGroups;
    }
}
