<?php include('../include/header.php') ?>

    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Финансы</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ul class="breadcrumb m-r-10">
                    <li><a href="/">Главная панель</a></li>
                    <li class="active">Финансы</li>
                    <li class="active">Добавление</li>
                </ul>
            </div>
            <!-- /.col-lg-12 -->
        </div>


        <div ng-controller="catalogController">
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Добавление новой сделки</h3>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <form>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Ссылка на сделку в Amo CRM</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1">
                                    </div>
                                    <div class="form-group">
                                        <label>Продукт</label>
                                            <select class="form-control">
                                                <option>Все продукты</option>
                                                <option>Долина</option>
                                                <option>Концентрат</option>
                                                <option>Маштабирование</option>
                                                <option>Акселерация</option>
                                                <option>Долина вип</option>
                                                <option>Наставничество</option>
                                                <option>Наставничество вип</option>
                                            </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Сумма</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1">
                                    </div>
                                    <div class="form-group">
                                        <label>Способ оплаты</label>
                                            <select class="form-control">
                                                <option>На карту</option>
                                                <option>В кассу</option>
                                                <option>Наличными</option>
                                                <option>Перевод с карты на карту</option>
                                            </select>
                                    </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">
                                        Сохранить
                                    </button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Отмена
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('../include/footer.php') ?>