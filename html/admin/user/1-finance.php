<?php include('../include/header.php') ?>

    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Финансы</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ul class="breadcrumb m-r-10">
                    <li><a href="/">Главная панель</a></li>
                    <li class="active">Финансы</li>
                </ul>
            </div>

            <!-- /.col-lg-12 -->
        </div>


        <div ng-controller="catalogController">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="white-box">
                            <div class="text-left pull-left">
                                <h1>Валовая выручка:</h1>
                            </div>
                            <div class="text-right">
                                <h1> $5,000</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel wallet-widgets">
                        <div class="panel-body">
                            <div class="text-left pull-left">
                                <h1>Безналичная выручка:</h1>
                            </div>
                            <div class="text-right">
                                <h1> $5,000</h1>
                            </div>
                        </div>
                        <ul class="wallet-list">
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Концентрат:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Долина:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Маштабиравание:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Акселерация:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Армия:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <button class="btn btn-block btn-info">ОБНОВИТЬ</button>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel wallet-widgets">
                        <div class="panel-body">
                            <div class="text-left pull-left">
                                <h1>Наличная выручка:</h1>
                            </div>
                            <div class="text-right">
                                <h1> $5,000</h1>
                            </div>
                        </div>
                        <ul class="wallet-list">
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Концентрат:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Долина:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Маштабиравание:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Акселерация:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                            <li><a href="javascript:void(0)">
                                    <div class="text-left pull-left">
                                        Армия:
                                    </div>
                                    <div class="text-right">
                                        $5,000
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <button class="btn btn-block btn-success">ДОБАВИТЬ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('../include/footer.php') ?>