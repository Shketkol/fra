<?php include('../include/header.php') ?>

    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Финансы</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ul class="breadcrumb m-r-10">
                    <li><a href="/">Главная панель</a></li>
                    <li class="active">Доход/Расход</li>
                </ul>
            </div>
            <!-- /.col-lg-12 -->
        </div>


        <div ng-controller="catalogController">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="white-box">
                            <div class="text-center">
                                <h1>Доход/Расход</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Доход</span></a></li>
                            <li role="presentation" class=""><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Расход</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="home">
                                <table class="table table-hover manage-u-table">
                                    <thead>
                                    <tr>
                                        <th>0-5</th>
                                        <th>6-11</th>
                                        <th>12-18</th>
                                        <th>19-25</th>
                                        <th>26-31</th>
                                        <th width="300">ИТОГО ЗА МЕСЯЦ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <table class="table table-hover manage-u-table">
                                    <thead>
                                    <tr>
                                        <th>0-5</th>
                                        <th>6-11</th>
                                        <th>12-18</th>
                                        <th>19-25</th>
                                        <th>26-31</th>
                                        <th width="300">ИТОГО ЗА МЕСЯЦ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

<?php include('../include/footer.php') ?>