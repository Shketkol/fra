<!DOCTYPE html>
<html lang="ru" ng-app="myApp">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Каталог</title>
    <link href="../../../plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
    <link href="../../../public/bootstrap/dist/css/bootstrap.min.css?v=1501067468" rel="stylesheet" position="1">
    <link href="../../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" position="1">
    <link href="../../../public/css/animate.css" rel="stylesheet" position="1">
    <link href="../../../public/css/style.css" rel="stylesheet" position="1">
    <link href="../../../public/css/main.css" rel="stylesheet" position="1">
    <link href="../../../public/css/colors/default.css" rel="stylesheet" position="1">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
</head>
<body ng-cloak ng-controller="mainController">


<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part top-main">
                <!-- Logo -->
                <a class="logo" href="index.html">
                    <span class="hidden-sm hidden-md hidden-lg">
                        Место в рейтинге по выручке: 13
                    </span>
                    <span class="hidden-xs">
                        Место в рейтинге по выручке: 13
                    </span>
                </a>
            </div>
            <!-- /Logo -->
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i
                                class="ti-close ti-menu"></i></a></li>
            </ul>
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li class="dropdown">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img
                                src="/plugins/images/users/varun.jpg" alt="user-img" width="36"
                                class="img-circle hidden"><b class="hidden-xs">Коля Коля</b><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <div class="dw-user-box p-b-5">
                                <div class="u-text">
                                    <h4>Коля Коля</h4>
                                    <p class="text-muted">
                                        Фирма </p>
                                    <p class="text-muted">Ваш ID: 24</p>
                                </div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#"><i class="ti-settings"></i>&nbsp; Настройки</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/logout"><i class="fa fa-sign-out"></i>&nbsp; Выйти</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>

                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav slimscrollsidebar">
            <div class="sidebar-head">
                <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span>
                    <span class="hide-menu">Навигация</span></h3></div>
            <ul class="nav" id="side-menu">
                <li><a href="/html/admin/main" class="waves-effect"><i class="mdi mdi-gauge fa-fw"></i><span
                                class="hide-menu">Главная панель</span></a></li>
                <li><a href="/catalog" class="waves-effect"><i class="mdi mdi-folder-multiple-outline fa-fw"></i><span
                                class="hide-menu">Финансы</span></a></li>
                <li><a href="/html/admin/orders" class="waves-effect"><i class="mdi mdi-cart-outline fa-fw"></i><span
                                class="hide-menu">Расчет роялти</span></a></li>
                <li><a href="/users" class="waves-effect"><i class="mdi mdi-account-multiple fa-fw"></i><span
                                class="hide-menu">Доход/Расход</span></a></li>
                <li><a href="/users" class="waves-effect"><i class="mdi mdi-account-multiple fa-fw"></i><span
                                class="hide-menu">Фин.модель</span></a></li>
                <li><a href="/users" class="waves-effect"><i class="mdi mdi-account-multiple fa-fw"></i><span
                                class="hide-menu">Планирование</span></a></li>
                <li><a href="/users" class="waves-effect"><i class="mdi mdi-account-multiple fa-fw"></i><span
                                class="hide-menu">Рейтинг</span></a></li>
                <li><a href="/html/admin/settings" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i><span
                                class="hide-menu">Настройки</span></a></li>
                <li><a href="/logout" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">Выйти</span></a>
                </li>
            </ul>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
