<?php include('include/header.php')?>

<div class="new-login-box">
    <div class="white-box">
        <h3 class="box-title m-b-0">Войти в кабинет</h3>
        <small>Введите Ваш e-mail и пароль</small>
        <form class="form-horizontal form-material new-lg-form" id="loginform" autocomplete="off" action="">

            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="E-mail">
                    <span class="help-block hidden"> E-mail не найден </span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" type="password" required="" placeholder="Пароль">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="checkbox checkbox-info pull-left p-t-0">
                        <input id="checkbox-signup" type="checkbox">
                        <label for="checkbox-signup"> Запомнить меня </label>
                    </div>
                    </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                            type="submit">Войти
                    </button>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                    <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p>Нет учетной записи? <a href="#" class="text-primary m-l-5"><b>Зарегистрируйтесь</b></a></p>
                </div>
            </div>
        </form>
    </div>
</div>

<?php include('include/footer.php')?>