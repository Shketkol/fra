<?php include('include/header.php')?>

    <div class="new-login-box">
    <div class="white-box">
        <h3 class="box-title m-b-0">Регистрация в кабинете</h3>
        <small>Пожалуйста, внесите свои данные.</small>
        <form class="form-horizontal form-material new-lg-form" id="loginform" autocomplete="off" action="">

            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="Имя">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="Фамилия">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder="Отчество">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder="ИНН">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="E-mail">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" type="password" required="" placeholder="Пароль">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" type="password" required="" placeholder="Подтвердите пароль">
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                            type="submit">Отправить заявку
                    </button>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p>Уже зарегистрированы? <a href="/html/login" class="text-primary m-l-5"><b>Войти в аккаунт</b></a></p>
                </div>
            </div>
        </form>
    </div>
</div>

<?php include('include/footer.php')?>