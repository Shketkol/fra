<?php

namespace common\models;

use common\components\traits\Validation;
use Yii;

/**
 * This is the model class for table "planing".
 *
 * @property integer $id
 * @property integer $users_id
 * @property string $name
 * @property string $content
 * @property string $year
 *
 * @property Users $users
 */
class Planing extends \yii\db\ActiveRecord
{
    use Validation;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'planing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'name', 'content', 'year'], 'required'],
            [['users_id'], 'integer'],
            [['year', 'content'], 'safe'],
            [['name'], 'string', 'max' => 300],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'name' => 'Название',
            'content' => 'Контент',
            'year' => 'Год',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    public function getArrayField(){
        return array('content');
    }
}
