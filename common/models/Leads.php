<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "leads".
 *
 * @property integer $id
 * @property integer $users_id
 * @property integer $products_id
 * @property string $name_lead
 * @property string $contact
 * @property string $state
 * @property string $price
 * @property string $email
 * @property string $phone
 * @property integer $type
 * @property string $date
 *
 * @property Users $users
 * @property Products $products
 */
class Leads extends \yii\db\ActiveRecord
{
    const TYPE_ONLINE = 1;
    const TYPE_OFFLINE = 2;

    public static $types = array(
        1 => 'на карту',
        2 => 'в кассу',
        3 => 'наличными',
        4 => 'перевод с карты на карту'
    );
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'products_id', 'name_lead', 'contact', 'state', 'price', 'email', 'phone', 'type', 'date'], 'required'],
            [['users_id', 'products_id', 'type', 'form_lead'], 'integer'],
            [['price'], 'number'],
            [['date'], 'safe'],
            [['name_lead', 'contact', 'state', 'email', 'phone', 'lead_url', 'status'], 'string', 'max' => 300],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['products_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['products_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'products_id' => 'Продукт',
            'name_lead' => 'Названиие сделки',
            'contact' => 'Контракт',
            'state' => 'Этап сделки',
            'price' => 'Сумма',
            'email' => 'Email',
            'phone' => 'Телефон',
            'type' => 'Тип',
            'date' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasOne(Products::className(), ['id' => 'products_id']);
    }

    public static function getSumProductUserOnline($user, $product, $year=false, $month=false, $start=false, $end=false, $leadStatus=false){
        //var_dump($start);
        
        $all_online = Leads::find()
            ->select('SUM(price) as value')
            ->where([
                'users_id' => $user,
                'type' => Leads::TYPE_ONLINE,
                'products_id' => $product
            ]);
        
        if($start && $end){
            $all_online = $all_online->andFilterWhere(['between', 'date',  $start, $end]);
        }
        
        if($leadStatus == 1) $all_online = $all_online->andFilterWhere(['status' => 'paid']);
        else if($leadStatus == 2) $all_online = $all_online->andFilterWhere(['status' => 'prepaid']);
        
        $all_online = $all_online->asArray()->one();
        
        /*
        if(!$year && !$month && !$start && !$end){
            var_dump('1');
            $all_online = Leads::find()
                ->select('SUM(price) as value')
                ->where([
                    'users_id' => $user,
                    'type' => Leads::TYPE_ONLINE,
                    'products_id' => $product
                ])
                ->asArray()
                ->one();
        }else if($year && $month) {
            var_dump('2');
            $all_online = Leads::find()
                ->select('SUM(price) as value')
                ->where([
                    'users_id' => $user,
                    'type' => Leads::TYPE_ONLINE,
                    'products_id' => $product
                ])
                ->andFilterWhere(['like', 'date',  $year.'-'.$month])
                ->asArray()
                ->one();
        }
        else{
            var_dump('3');
            $all_online = Leads::find()
                ->select('SUM(price) as value')
                ->where([
                    'users_id' => $user,
                    'type' => Leads::TYPE_ONLINE,
                    'products_id' => $product
                ])
                ->andFilterWhere(['between', 'date',  $start, $end])
                ->asArray()
                ->one();
        }
        */
        return (empty($all_online['value'])) ? '0.00' : $all_online['value'];
    }

    public static function getSumProductUserOffline($user, $product, $year=false, $month=false, $start=false, $end=false){
        if(!$start && !$end){
            $all_online = Leads::find()
                ->select('SUM(price) as value')
                ->where([
                    'users_id' => $user,
                    'type' => Leads::TYPE_OFFLINE,
                    'products_id' => $product
                ])
                ->asArray()
                ->one();
        }
        else{
            $all_online = Leads::find()
                ->select('SUM(price) as value')
                ->where([
                    'users_id' => $user,
                    'type' => Leads::TYPE_OFFLINE,
                    'products_id' => $product
                ])
                ->andFilterWhere(['between', 'date',  $start, $end])
                ->asArray()
                ->one();
            
        }
        
        /*
        if(!$year && !$month && !$start && !$end){
            $all_online = Leads::find()
                ->select('SUM(price) as value')
                ->where([
                    'users_id' => $user,
                    'type' => Leads::TYPE_OFFLINE,
                    'products_id' => $product
                ])
                ->asArray()
                ->one();
        }else if($year && $month) {
            $all_online = Leads::find()
                ->select('SUM(price) as value')
                ->where([
                    'users_id' => $user,
                    'type' => Leads::TYPE_OFFLINE,
                    'products_id' => $product
                ])
                ->andFilterWhere(['like', 'date',  $year.'-'.$month])
                ->asArray()
                ->one();
        }
        else{
            $all_online = Leads::find()
                ->select('SUM(price) as value')
                ->where([
                    'users_id' => $user,
                    'type' => Leads::TYPE_OFFLINE,
                    'products_id' => $product
                ])
                ->andFilterWhere(['between', 'date',  $start, $end])
                ->asArray()
                ->one();
            
        }
        */
        return (empty($all_online['value'])) ? '0.00' : $all_online['value'];
    }
}
