<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "users_raiting".
 *
 * @property string $surname
 * @property string $patronymic
 * @property integer $id
 * @property string $name
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $users_roles_id
 * @property integer $email_confirmed
 * @property string $amo_domain
 * @property string $amo_login
 * @property string $amo_hash
 * @property integer $city_people_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $price
 */
class UsersRaiting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_raiting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'users_roles_id', 'email_confirmed', 'city_people_id'], 'integer'],
            [['auth_key', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['price'], 'number'],
            [['surname', 'patronymic', 'name', 'password_hash', 'password_reset_token', 'email', 'amo_domain', 'amo_login', 'amo_hash'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'surname' => 'Город',
            'patronymic' => 'Отчество',
            'id' => 'ID',
            'name' => 'Имя',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Статус',
            'users_roles_id' => 'Роль',
            'email_confirmed' => 'Email',
            'amo_domain' => 'Amo Domain',
            'amo_login' => 'Amo Login',
            'amo_hash' => 'Amo Hash',
            'city_people_id' => 'Город',
            'created_at' => 'Дата',
            'updated_at' => 'Дата',
            'price' => 'Сумма',
        ];
    }
}
