<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "outgo_users".
 *
 * @property integer $id
 * @property integer $users_id
 * @property integer $outgo_id
 * @property integer $week
 * @property string $value
 * @property string $year
 *
 * @property Outgo $outgo
 * @property Users $users
 */
class OutgoUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outgo_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'outgo_id', 'week', 'value', 'year'], 'required'],
            [['users_id', 'outgo_id', 'week', 'month'], 'integer'],
            [['value'], 'number'],
            [['year'], 'safe'],
            [['outgo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Outgo::className(), 'targetAttribute' => ['outgo_id' => 'id']],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'outgo_id' => 'Вид расхода',
            'week' => 'Неделя',
            'value' => 'Значение',
            'year' => 'Год',
            'month' => 'Месяц',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgo()
    {
        return $this->hasOne(Outgo::className(), ['id' => 'outgo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    public static function getIncome($user, $month, $year, $income_field, $week){
        $model = OutgoUsers::findOne([
            'users_id' => $user,
            'month' => $month,
            'year' => $year,
            'outgo_id' => $income_field,
            'week' => $week
        ]);
        return $model;
    }
}
