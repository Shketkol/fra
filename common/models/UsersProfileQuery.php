<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[UsersProfile]].
 *
 * @see UsersProfile
 */
class UsersProfileQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UsersProfile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UsersProfile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
