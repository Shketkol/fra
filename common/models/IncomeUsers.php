<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "income_users".
 *
 * @property integer $id
 * @property integer $users_id
 * @property integer $income_field
 * @property integer $week
 * @property string $value
 *
 * @property Users $users
 * @property IncomeField $incomeField
 */
class IncomeUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'income_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'income_field', 'week', 'value'], 'required'],
            [['id', 'users_id', 'income_field', 'week', 'month'], 'integer'],
            [['value', 'year'], 'number'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['income_field'], 'exist', 'skipOnError' => true, 'targetClass' => IncomeField::className(), 'targetAttribute' => ['income_field' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'income_field' => 'Поле дохода',
            'week' => 'Неделя',
            'value' => 'Значение',
            'year' => 'Год',
            'month' => 'Месяц',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeField()
    {
        return $this->hasOne(IncomeField::className(), ['id' => 'income_field']);
    }

    public static function getIncome($user, $month, $year, $income_field, $week){
        $model = IncomeUsers::findOne([
            'users_id' => $user,
            'month' => $month,
            'year' => $year,
            'income_field' => $income_field,
            'week' => $week
        ]);
        return $model;
    }
}
