<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "city_people".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ProductCitiesPercent[] $productCitiesPercents
 * @property Users[] $users
 */
class CityPeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCitiesPercents()
    {
        return $this->hasMany(ProductCitiesPercent::className(), ['city_people_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['city_people_id' => 'id']);
    }
}
