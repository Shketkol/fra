<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "royalty".
 *
 * @property integer $id
 * @property integer $users_id
 * @property string $value
 * @property integer $agree
 * @property string $created_at
 *
 * @property Users $users
 */
class Royalty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'royalty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'value'], 'required'],
            [['users_id', 'agree'], 'integer'],
            [['value'], 'number'],
            [['comment'], 'string'],
            [['created_at'], 'safe'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'value' => 'Значения',
            'agree' => 'Согласие',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    public static function getRoyaltyForUser($user, $product, $year, $month){
        $online = Leads::find()
            ->select('SUM(price) as price')
            ->where(['users_id' => $user, 'products_id' => $product->id, 'type' => Leads::TYPE_ONLINE])
            ->andFilterWhere(['like', 'date', $year.'-'.$month])
            ->asArray()
            ->one();
        $online = (empty($online['price'])) ? 0.00 : $online['price'];

        $offline = Leads::find()
            ->select('SUM(price) as price')
            ->where(['users_id' => $user, 'products_id' => $product->id, 'type' => Leads::TYPE_OFFLINE])
            ->andFilterWhere(['like', 'date',  $year.'-'.$month])
            ->asArray()
            ->one();
        $offline = (empty($offline['price'])) ? 0.00 : $offline['price'];

        $user = Users::findOne(['id' => $user]);
        $percent = ProductCitiesPercent::find()
                ->joinWith('products')
                ->where(['city_people_id' => $user->city_people_id])
                ->andWhere(['products.name' => $product->name])
                ->one();
        if(!empty($percent)){
            $transh = ($online + $offline) - ($online * 0.05);
            $royalti = number_format($transh * $percent->value/100 - $offline, 2, '.', '');
        }else {
            $royalti = '0.00';
        }
        return $royalti;
    }

    public static function getAllLeads($user, $product, $year, $month){
        $online = Leads::find()
            ->where(['users_id' => $user, 'products_id' => $product->id, 'type' => Leads::TYPE_ONLINE])
            ->andFilterWhere(['like', 'date', $year.'-'.$month])
            ->count();

        $offline = Leads::find()
            ->where(['users_id' => $user, 'products_id' => $product->id, 'type' => Leads::TYPE_OFFLINE])
            ->andFilterWhere(['like', 'date',  $year.'-'.$month])
            ->count();
        return $online + $offline;
    }

    public static function getAllRoyailty($user_id, $year, $month){
        $royalti = 0.00;
        foreach (Products::find()->all() as $product){
            $royalti = $royalti + self::getRoyaltyForUser($user_id, $product, $year, $month);
        }
        return $royalti;
    }

}
