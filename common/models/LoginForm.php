<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            [['email'], 'email'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['email', 'validateStatus'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('common', 'Неверный e-mail или пароль');
            }else {
                return true;
            }
        }
    }

    public function validateStatus()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!in_array($user->status, [Users::STATUS_ACTIVE])) {
                $this->addError('common', 'Аккаунт не активирован. Обратитесь в слубжу поддержки.');
            }else {
                return true;
            }
        }
    }


    public function validateEmail()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || $user->email_confirmed == 0) {
                $this->addError('email', 'Ваш e-mail не подтвержден');
                return false;
            }else {
                return true;
            }
        }
    }

    public function getStatus()
    {
        $user = $this->getUser();
        return $user->status;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Users::findByEmail($this->email);
        }
        return $this->_user;
    }
}
