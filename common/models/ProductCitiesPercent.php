<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_cities_percent".
 *
 * @property integer $id
 * @property integer $products_id
 * @property integer $city_people_id
 * @property string $value
 *
 * @property Products $products
 * @property CityPeople $cityPeople
 */
class ProductCitiesPercent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_cities_percent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['products_id', 'city_people_id', 'value'], 'required'],
            [['products_id', 'city_people_id'], 'integer'],
            [['value'], 'number'],
            [['products_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['products_id' => 'id']],
            [['city_people_id'], 'exist', 'skipOnError' => true, 'targetClass' => CityPeople::className(), 'targetAttribute' => ['city_people_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'products_id' => 'Продукт',
            'city_people_id' => 'Город',
            'value' => 'Значение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasOne(Products::className(), ['id' => 'products_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityPeople()
    {
        return $this->hasOne(CityPeople::className(), ['id' => 'city_people_id']);
    }

    public static function getPercent($product, $user){
        $user = Users::findOne(['id' => $user]);
        $percent = ProductCitiesPercent::find()
            ->joinWith('products')
            ->where(['city_people_id' => $user->city_people_id])
            ->andWhere(['products.name' => $product->name])
            ->one();
        return (!empty($percent)) ? $percent->value : 0;

    }
}
