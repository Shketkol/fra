<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class LeadsSearch extends \common\models\Leads
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'users_id', 'products_id', 'type', 'amo_id'], 'integer'],
            [['name_lead', 'contact', 'state', 'email', 'phone', 'date', 'price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $user, $product, $type=null, $start=null, $end=null, $leadStatus=null)
    {
        $query = Leads::find();
        if(is_null($type)){
            $query->where(['users_id' => $user, 'products_id' => $product]);
        }else {
            $query->where(['users_id' => $user, 'products_id' => $product, 'type' => $type]);
        }
        
        if(!is_null($start) & !is_null($end)){
            $query->andFilterWhere(['between', 'date',  $start, $end]);
        }
        
        if(!is_null($leadStatus)){
            if($leadStatus == 1) $query->andFilterWhere(['status' => 'paid']);
            else if($leadStatus == 2) $query->andFilterWhere(['status' => 'prepaid']);
        }
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['date' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'users_id' => $this->users_id,
            'products_id' => $this->products_id,
            'amo_id' => $this->amo_id,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'name_lead', $this->name_lead])
            ->andFilterWhere(['like', 'contact', $this->contact])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'date', $this->date]);

        return $dataProvider;
    }
}
