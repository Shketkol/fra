<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => require(dirname(__DIR__) . "/config/db.php"),

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.gmail.com',
//                'username' => 'webra.dev@gmail.com',
//                'password' => 'webra.devwebra.dev',
//                'port' => '587',
//                'encryption' => 'tls',
//            ],
            'useFileTransport' => false,
        ],
    ],
];
